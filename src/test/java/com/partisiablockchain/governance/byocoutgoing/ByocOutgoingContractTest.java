package com.partisiablockchain.governance.byocoutgoing;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.ContractEventInteraction;
import com.partisiablockchain.contract.SysContractContextTest;
import com.partisiablockchain.contract.SysContractSerialization;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Tests. */
public final class ByocOutgoingContractTest {

  private final SysContractSerialization<ByocOutgoingContractState> serialization =
      new SysContractSerialization<>(
          ByocOutgoingContractInvoker.class, ByocOutgoingContractState.class);
  private SysContractContextTest context;

  private final KeyPair oracleKeyPair0 = new KeyPair(BigInteger.ONE);
  private final KeyPair oracleKeyPair1 = new KeyPair(BigInteger.TWO);
  private final KeyPair oracleKeyPair2 = new KeyPair(BigInteger.TEN);
  private final KeyPair oracleAccountKey0 = new KeyPair(BigInteger.valueOf(112233));
  private final KeyPair oracleAccountKey1 = new KeyPair(BigInteger.valueOf(2233));
  private final KeyPair oracleAccountKey2 = new KeyPair(BigInteger.valueOf(1234578));
  private final KeyPair unknownOracleKeyPair = new KeyPair(BigInteger.valueOf(1337L));
  private final BlockchainPublicKey oracleKey0 = oracleKeyPair0.getPublic();
  private final BlockchainPublicKey oracleKey1 = oracleKeyPair1.getPublic();
  private final BlockchainPublicKey oracleKey2 = oracleKeyPair2.getPublic();
  private final BlockchainAddress oracle0 = oracleAccountKey0.getPublic().createAddress();
  private final BlockchainAddress oracle1 = oracleAccountKey1.getPublic().createAddress();
  private final BlockchainAddress oracle2 = oracleAccountKey2.getPublic().createAddress();
  private final List<BlockchainAddress> oracles = List.of(oracle0, oracle1, oracle2);

  private final BlockchainAddress largeOracleContract =
      BlockchainAddress.fromString("040000000000000000000000000000000000000001");
  private final BlockchainAddress votingContract =
      BlockchainAddress.fromString("040000000000000000000000000000000000000002");
  private final EthereumAddress byocContract =
      EthereumAddress.fromString("0000000000000000000000000000000000000001");
  private static final String symbol = "TEST_SYMBOL";
  private final BlockchainAddress account1 =
      BlockchainAddress.fromString("000000000000000000000000000000000000000011");
  private final BlockchainAddress account2 =
      BlockchainAddress.fromString("000000000000000000000000000000000000000022");
  private final EthereumAddress receiver =
      EthereumAddress.fromString("0000000000000000000000000000000000000003");
  private final EthereumAddress receiver2 =
      EthereumAddress.fromString("0000000000000000000000000000000000000004");
  private final EthereumAddress receiver3 =
      EthereumAddress.fromString("0000000000000000000000000000000000000005");
  private static final Unsigned256 amount = Unsigned256.create(12345678L);
  private static final Unsigned256 withdrawMinimum = Unsigned256.create(2);
  private static final Unsigned256 totalWithdrawalMaximum = amount.multiply(Unsigned256.create(5));
  private static final Hash requestingTransaction = Hash.create(s -> s.writeLong(112233L));
  private static final byte INVALID_INVOCATION_BYTE = 100;

  private ByocOutgoingContractState state;

  static List<OracleMember> generateNewOracles() {
    List<OracleMember> result = new ArrayList<>();
    for (int i = 0; i < 3; i++) {
      BlockchainPublicKey publicKey = new KeyPair().getPublic();
      result.add(new OracleMember(publicKey.createAddress(), publicKey));
    }
    return result;
  }

  /** Initial state and mocks. */
  @BeforeEach
  public void setUp() {
    context = new SysContractContextTest(1, 100, account1);
    state =
        serialization.create(
            context,
            stream -> {
              largeOracleContract.write(stream);
              byocContract.write(stream);
              stream.writeString(symbol);
              BlockchainAddress.LIST_SERIALIZER.writeDynamic(stream, oracles);
              BlockchainPublicKey.LIST_SERIALIZER.writeDynamic(
                  stream, List.of(oracleKey0, oracleKey1, oracleKey2));
              withdrawMinimum.write(stream);
              totalWithdrawalMaximum.write(stream);
            });
  }

  @Test
  public void create() {
    assertThat(state.getLargeOracleContract()).isEqualTo(largeOracleContract);
    assertThat(state.getByocContract().getIdentifier()).isEqualTo(byocContract.getIdentifier());
    assertThat(state.getSymbol()).isEqualTo(symbol);
    assertThat(state.getOracleNodes()).isEqualTo(oracles);
    assertThat(state.getOracleMembers())
        .extracting(OracleMember::getKey)
        .isEqualTo(List.of(oracleKey0, oracleKey1, oracleKey2));
    assertThat(state.getWithdrawNonce()).isEqualTo(0L);
    assertThat(state.getMaximumWithdrawalPerEpoch()).isEqualTo(totalWithdrawalMaximum);
  }

  @Test
  public void createWithoutOracle() {
    state =
        serialization.create(
            context,
            stream -> {
              largeOracleContract.write(stream);
              byocContract.write(stream);
              stream.writeString(symbol);
              BlockchainAddress.LIST_SERIALIZER.writeDynamic(stream, List.of());
              BlockchainPublicKey.LIST_SERIALIZER.writeDynamic(stream, List.of());
              withdrawMinimum.write(stream);
              totalWithdrawalMaximum.write(stream);
            });
    assertThat(state.getLargeOracleContract()).isEqualTo(largeOracleContract);
    assertThat(state.getByocContract().getIdentifier()).isEqualTo(byocContract.getIdentifier());
    assertThat(state.getSymbol()).isEqualTo(symbol);
    assertThat(state.getOracleNodes()).isEmpty();
    assertThat(state.getOracleMembers()).isEmpty();
    assertThat(state.getWithdrawNonce()).isEqualTo(0L);
    assertThat(state.getMaximumWithdrawalPerEpoch()).isEqualTo(totalWithdrawalMaximum);
    assertThat(state.getCurrentEpoch()).isEqualTo(1L);
  }

  @Test
  public void invokeInvalidInvocation() {
    assertThatThrownBy(
            () ->
                serialization.invoke(context, state, rpc -> rpc.writeByte(INVALID_INVOCATION_BYTE)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid shortname: 100");
  }

  @Test
  public void createWithIdentitiesAndKeyListLengthDifferingFails() {
    assertThatThrownBy(
            () ->
                serialization.create(
                    context,
                    stream -> {
                      largeOracleContract.write(stream);
                      byocContract.write(stream);
                      stream.writeString(symbol);
                      BlockchainAddress.LIST_SERIALIZER.writeDynamic(stream, oracles);
                      BlockchainPublicKey.LIST_SERIALIZER.writeDynamic(
                          stream, List.of(oracleKey0, oracleKey1));
                      Unsigned256.ZERO.write(stream);
                      Unsigned256.ZERO.write(stream);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Lengths of oracle identities and keys did not match");
  }

  @Test
  public void addPendingWithdrawal() {
    assertThat(state.getWithdrawals().size()).isEqualTo(0);
    assertThat(state.getWithdrawNonce()).isEqualTo(0L);
    from(account1);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(ByocOutgoingContract.Invocations.ADD_PENDING_WITHDRAWAL);
              receiver.write(rpc);
              amount.write(rpc);
            });
    assertThat(state.getWithdrawals().size()).isEqualTo(0);
    assertThat(state.getWithdrawNonce()).isEqualTo(0L);
    byte[] actualRpc = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] expectedRpc = AccountPluginRpc.deductCoinBalance(symbol, amount);
    assertThat(actualRpc).isEqualTo(expectedRpc);
    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            ByocOutgoingContract.Callbacks.createWithdrawal(account1, receiver, amount));
    assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
  }

  @Test
  public void addingPendingWithdrawalFailsWhenAmountIsBelowMinimum() {
    from(account1);
    assertThatThrownBy(
            () ->
                state =
                    serialization.invoke(
                        context,
                        state,
                        rpc -> {
                          rpc.writeByte(ByocOutgoingContract.Invocations.ADD_PENDING_WITHDRAWAL);
                          receiver.write(rpc);
                          withdrawMinimum.subtract(Unsigned256.ONE).write(rpc);
                        }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Amount must be larger than or equal the minimum: " + withdrawMinimum);

    assertThatCode(
            () ->
                state =
                    serialization.invoke(
                        context,
                        state,
                        rpc -> {
                          rpc.writeByte(ByocOutgoingContract.Invocations.ADD_PENDING_WITHDRAWAL);
                          receiver.write(rpc);
                          withdrawMinimum.write(rpc);
                        }))
        .doesNotThrowAnyException();
  }

  @Test
  public void addingPendingWithdrawalFailsWhenAmountIsZero() {
    from(account1);
    assertThatThrownBy(
            () ->
                state =
                    serialization.invoke(
                        context,
                        state,
                        rpc -> {
                          rpc.writeByte(ByocOutgoingContract.Invocations.ADD_PENDING_WITHDRAWAL);
                          receiver.write(rpc);
                          Unsigned256.ZERO.write(rpc);
                        }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Amount must be larger than or equal the minimum: " + withdrawMinimum);
  }

  @Test
  public void addPendingWithdrawalFailWhenAmountIsAboveMaximum() {
    from(account1);
    assertThatThrownBy(
            () ->
                state =
                    serialization.invoke(
                        context,
                        state,
                        rpc -> {
                          rpc.writeByte(ByocOutgoingContract.Invocations.ADD_PENDING_WITHDRAWAL);
                          receiver.write(rpc);
                          totalWithdrawalMaximum.multiply(Unsigned256.create(2)).write(rpc);
                        }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Amount must be less than or equal to the maximum withdrawal per epoch: "
                + totalWithdrawalMaximum);
  }

  @Test
  public void addPendingWithdrawalWhenAmountIsMaximum() {
    assertThat(state.getWithdrawals().size()).isEqualTo(0);
    assertThat(state.getWithdrawNonce()).isEqualTo(0L);
    from(account1);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(ByocOutgoingContract.Invocations.ADD_PENDING_WITHDRAWAL);
              receiver.write(rpc);
              totalWithdrawalMaximum.write(rpc);
            });
    assertThat(state.getWithdrawals().size()).isEqualTo(0);
    assertThat(state.getWithdrawNonce()).isEqualTo(0L);
    byte[] actualRpc = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] expectedRpc = AccountPluginRpc.deductCoinBalance(symbol, totalWithdrawalMaximum);
    assertThat(actualRpc).isEqualTo(expectedRpc);
    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            ByocOutgoingContract.Callbacks.createWithdrawal(
                account1, receiver, totalWithdrawalMaximum));
    assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
  }

  @Test
  public void callback() {
    CallbackContext callbackContext = CallbackContext.create(FixedList.create(List.of()));
    assertThat(state.getWithdrawNonce()).isEqualTo(0L);
    state =
        serialization.callback(
            context,
            callbackContext,
            state,
            data -> {
              data.writeByte(ByocOutgoingContract.Callbacks.CREATE_WITHDRAWAL);
              account1.write(data);
              receiver.write(data);
              amount.write(data);
            });

    state =
        serialization.callback(
            context,
            callbackContext,
            state,
            data -> {
              data.writeByte(ByocOutgoingContract.Callbacks.CREATE_WITHDRAWAL);
              account1.write(data);
              receiver.write(data);
              amount.multiply(Unsigned256.create(2)).write(data);
            });

    assertThat(state.getWithdrawNonce()).isEqualTo(2L);

    Withdrawal withdrawal = state.getPendingWithdrawal(0, 0L);
    assertThat(withdrawal).isNotNull();
    assertThat(withdrawal.getSignatures()).containsExactly(null, null, null);
    assertThat(withdrawal.getReceiver().getIdentifier()).isEqualTo(receiver.getIdentifier());
    assertThat(withdrawal.getAmount()).isEqualTo(amount.subtract(tax(amount)));
    assertThat(StateAccessor.create(withdrawal).get("requestingTransaction").hashValue())
        .isEqualTo(Hash.create(stream -> stream.writeLong(context.getBlockTime())));

    withdrawal = state.getPendingWithdrawal(0, 1L);
    assertThat(withdrawal).isNotNull();
    assertThat(withdrawal.getSignatures()).containsExactly(null, null, null);
    assertThat(withdrawal.getReceiver().getIdentifier()).isEqualTo(receiver.getIdentifier());
    Unsigned256 otherAmount = amount.multiply(Unsigned256.create(2));
    assertThat(withdrawal.getAmount()).isEqualTo(otherAmount.subtract(tax(otherAmount)));
  }

  @Test
  public void invocationDoesNotExist() {
    CallbackContext callbackContext = CallbackContext.create(FixedList.create(List.of()));
    assertThatThrownBy(
            () ->
                serialization.callback(
                    context,
                    callbackContext,
                    state,
                    stream -> {
                      stream.writeByte(10);
                      BlockchainAddress.fromString("040000000000000000000000000000000000000001")
                          .write(stream);
                      receiver.write(stream);
                      amount.write(stream);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid shortname: 10");
  }

  @Test
  public void createWithdrawalCallback() {
    CallbackContext callbackContext = CallbackContext.create(FixedList.create(List.of()));
    state =
        serialization.callback(
            context,
            callbackContext,
            state,
            stream -> {
              stream.writeByte(5);
              BlockchainAddress.fromString("040000000000000000000000000000000000000001")
                  .write(stream);
              receiver.write(stream);
              amount.write(stream);
            });
    assertThat(state.getWithdrawalSum()).isEqualTo(amount);
  }

  @Test
  public void callbackBumpsWithdrawSumAndRequestsNewOracleWhenMaxPerEpochIsReached() {
    CallbackContext callbackContext = CallbackContext.create(FixedList.create(List.of()));

    assertThat(state.getWithdrawalSum()).isEqualTo(Unsigned256.ZERO);
    state =
        serialization.callback(
            context,
            callbackContext,
            state,
            stream -> {
              stream.writeByte(ByocOutgoingContract.Callbacks.CREATE_WITHDRAWAL);
              account1.write(stream);
              receiver.write(stream);
              amount.write(stream);
            });

    assertThat(state.getWithdrawalSum()).isEqualTo(amount);
    Unsigned256 otherAmount = totalWithdrawalMaximum.subtract(amount);
    state =
        serialization.callback(
            context,
            callbackContext,
            state,
            stream -> {
              stream.writeByte(ByocOutgoingContract.Callbacks.CREATE_WITHDRAWAL);
              account1.write(stream);
              receiver.write(stream);
              otherAmount.write(stream);
            });

    assertThat(state.getWithdrawalSum()).isEqualTo(totalWithdrawalMaximum);

    // Hash of the two first withdrawals. Third withdrawal is handled by the next oracle.
    // The hashes of the merkle tree should be pair-sorted, thus the second withdrawal comes first.
    Hash merkleTree =
        Hash.create(
            stream -> {
              Hash.create(
                      first -> {
                        first.writeLong(0L);
                        first.write(receiver.getIdentifier());
                        amount.subtract(tax(amount)).write(first);
                      })
                  .write(stream);
              Hash.create(
                      second -> {
                        second.writeLong(1L);
                        second.write(receiver.getIdentifier());
                        otherAmount.subtract(tax(otherAmount)).write(second);
                      })
                  .write(stream);
            });

    state =
        serialization.callback(
            context,
            callbackContext,
            state,
            stream -> {
              stream.writeByte(ByocOutgoingContract.Callbacks.CREATE_WITHDRAWAL);
              account1.write(stream);
              receiver.write(stream);
              Unsigned256.ONE.write(stream);
            });

    byte[] smallOracleContext =
        SafeDataOutputStream.serialize(
            stream -> {
              state.getByocContract().write(stream);
              stream.writeLong(0);
              merkleTree.write(stream);
            });
    ContractEventInteraction callbackInteraction =
        (ContractEventInteraction) context.getInteractions().get(0);
    byte[] actualRpc = SafeDataOutputStream.serialize(callbackInteraction.rpc);
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(LargeOracleRpc.requestNewSmallOracle(smallOracleContext));
    assertThat(actualRpc).isEqualTo(expectedRpc);
  }

  @Test
  public void callbackFailed() {
    CallbackContext callbackContext =
        CallbackContext.create(
            FixedList.create(
                List.of(CallbackContext.createResult(requestingTransaction, false, emptyRpc()))));

    assertThatThrownBy(
            () ->
                serialization.callback(
                    context,
                    callbackContext,
                    state,
                    data -> {
                      data.writeByte(ByocOutgoingContract.Callbacks.CREATE_WITHDRAWAL);
                      account1.write(data);
                      receiver.write(data);
                      amount.write(data);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(String.format("Unable to withdraw %s %s from %s", amount, symbol, account1));
  }

  @Test
  public void signPendingWithdrawal() {
    state = state.createPendingWithdrawal(requestingTransaction, receiver, amount);

    Withdrawal withdrawal = state.getPendingWithdrawal(0, 0);
    assertThat(withdrawal.getSignatures()).hasSize(3);
    assertThat(withdrawal.getSignatures()).allMatch(Objects::isNull);
    FixedList<BlockchainAddress> signers = withdrawal.getSigners(oracles);
    assertThat(signers).isEmpty();

    Signature signature0 = addSignature(oracle0, oracleKeyPair0, 0);
    withdrawal = state.getPendingWithdrawal(0, 0);
    List<Signature> signatures = withdrawal.getSignatures();
    assertThat(signatures.get(0)).isEqualTo(signature0);
    signers = withdrawal.getSigners(oracles);
    assertThat(signers).containsExactly(oracle0);

    final Signature signature2 = addSignature(oracle2, oracleKeyPair2, 0);
    withdrawal = state.getPendingWithdrawal(0, 0);
    signatures = withdrawal.getSignatures();
    assertThat(signatures.get(0)).isEqualTo(signature0);
    assertThat(signatures.get(2)).isEqualTo(signature2);
    signers = withdrawal.getSigners(oracles);
    assertThat(signers).containsExactly(oracle0, oracle2);
  }

  @Test
  public void addSignatureFailsWhenWithdrawalWithNonceDoesNotExist() {
    state = state.createPendingWithdrawal(requestingTransaction, receiver, amount);

    assertThatThrownBy(() -> addSignature(oracle0, 1337, randomSignature()))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Withdrawal with nonce 1337 does not exist");
  }

  @Test
  public void addSignatureFailsWhenOracleIsUnknown() {
    state = state.createPendingWithdrawal(requestingTransaction, receiver, amount);
    BlockchainAddress sender = unknownOracleKeyPair.getPublic().createAddress();
    assertThatThrownBy(() -> addSignature(sender, unknownOracleKeyPair, 0))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Signature did not come from an oracle node");
  }

  @Test
  public void addSignatureFailsWhenSenderIsNotEqualToSigner() {
    state = state.createPendingWithdrawal(requestingTransaction, receiver, amount);
    Withdrawal withdrawal = state.getWithdrawals().getValue(0L);
    Hash hash = state.messageDigest(0, 0, withdrawal);
    Signature sig = oracleKeyPair0.sign(hash);
    from(oracle2);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(ByocOutgoingContract.Invocations.SIGN_PENDING_WITHDRAWAL);
                      rpc.writeLong(0);
                      rpc.writeLong(0);
                      sig.write(rpc);
                    }))
        .hasMessage("Invalid signature");
  }

  @Test
  public void testMessageDigest() {
    state = state.createPendingWithdrawal(requestingTransaction, receiver2, amount);
    long oracleNonce = 10;
    long nonce = 37;
    Withdrawal withdrawal = state.getWithdrawals().getValue(0L);
    Hash messageDigest = state.messageDigest(oracleNonce, nonce, withdrawal);
    Hash hash =
        Hash.create(
            stream -> {
              byocContract.write(stream);
              stream.writeLong(oracleNonce);
              stream.writeLong(nonce);
              withdrawal.getReceiver().write(stream);
              withdrawal.getAmount().write(stream);
            });

    assertThat(messageDigest).isEqualTo(hash);
  }

  private Signature addSignature(BlockchainAddress sender, KeyPair signer, long nonce) {
    Hash hash = state.messageDigest(0, nonce, state.getPendingWithdrawal(0L, nonce));
    Signature sig = signer.sign(hash);
    addSignature(sender, nonce, sig);
    return sig;
  }

  private void addSignature(BlockchainAddress sender, long nonce, Signature sig) {
    from(sender);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(ByocOutgoingContract.Invocations.SIGN_PENDING_WITHDRAWAL);
              rpc.writeLong(0);
              rpc.writeLong(nonce);
              sig.write(rpc);
            });
  }

  private Signature randomSignature() {
    Random rng = new Random();
    int recoveryId = rng.nextInt(2) + 1;
    BigInteger valueR = new BigInteger(32, rng);
    BigInteger valueS = new BigInteger(32, rng);
    return new Signature(recoveryId, valueR, valueS);
  }

  @Test
  public void disputeWithdrawal() {
    long oracleNonce = 0L;
    long nonce = 0L;
    state = state.createPendingWithdrawal(requestingTransaction, receiver, amount);
    Dispute.Transaction transaction =
        new Dispute.Transaction(receiver, amount.subtract(Unsigned256.ONE), 0b111);
    from(account1);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(ByocOutgoingContract.Invocations.DISPUTE_CREATE);
              rpc.writeLong(oracleNonce);
              rpc.writeLong(nonce);
              transaction.write(rpc);
            });

    assertDisputeCreated(new WithdrawId(nonce, oracleNonce), transaction);
  }

  @Test
  public void cannotDisputeIfTransactionIsEqualToWithdrawalStoredInState() {
    state = state.createPendingWithdrawal(requestingTransaction, receiver, amount);
    Dispute.Transaction transaction = new Dispute.Transaction(receiver, withoutTax(amount), 0b111);
    from(account1);
    assertThatThrownBy(
            () ->
                state =
                    serialization.invoke(
                        context,
                        state,
                        rpc -> {
                          rpc.writeByte(ByocOutgoingContract.Invocations.DISPUTE_CREATE);
                          rpc.writeLong(0);
                          rpc.writeLong(0);
                          transaction.write(rpc);
                        }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Disputed transaction is equal to the withdrawal stored in state");
  }

  @Test
  public void disputeWithdrawalWithNoExistingPbcCounterPart() {
    long withdrawalNonce = 0L;
    long oracleNonce = 0L;
    Dispute.Transaction transaction =
        new Dispute.Transaction(receiver, amount.subtract(Unsigned256.ONE), 0b111);
    from(account1);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(ByocOutgoingContract.Invocations.DISPUTE_CREATE);
              rpc.writeLong(oracleNonce);
              rpc.writeLong(withdrawalNonce);
              transaction.write(rpc);
            });

    assertDisputeCreated(new WithdrawId(oracleNonce, withdrawalNonce), transaction);
  }

  @Test
  public void addMultipleActiveDisputes() {
    long withdrawalOne = 0;
    long withdrawalTwo = 1;
    long oracleNonce = 0L;
    Dispute.Transaction tx1 = new Dispute.Transaction(receiver, amount, 0b111);
    Dispute.Transaction tx2 = new Dispute.Transaction(receiver, amount, 0b111);
    WithdrawId id1 = new WithdrawId(oracleNonce, withdrawalOne);
    WithdrawId id2 = new WithdrawId(oracleNonce, withdrawalTwo);
    state =
        state
            .createDispute(id1, Dispute.create(account1, tx1))
            .archiveActiveDispute(id1, 0)
            .createDispute(id2, Dispute.create(account2, tx2))
            .archiveActiveDispute(id2, 0);

    Dispute.Transaction newDispute = new Dispute.Transaction(receiver, amount, 0b111);
    from(account1);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(ByocOutgoingContract.Invocations.DISPUTE_CREATE);
              rpc.writeLong(0);
              rpc.writeLong(2);
              newDispute.write(rpc);
            });
    WithdrawId newId = new WithdrawId(oracleNonce, 2);
    assertDisputeCreated(newId, newDispute);
  }

  private void assertDisputeCreated(WithdrawId id, Dispute.Transaction transaction) {
    Dispute dispute = state.getDispute(id.oracleNonce(), id.withdrawalNonce());
    assertThat(dispute.getChallenger()).isEqualTo(account1);
    assertThat(dispute.getClaims().get(0)).isEqualTo(transaction);

    ContractEventInteraction eventInteraction =
        (ContractEventInteraction) context.getInteractions().get(0);
    assertThat(eventInteraction.contract).isEqualTo(largeOracleContract);
    byte[] actualRpc = SafeDataOutputStream.serialize(eventInteraction.rpc);
    byte[] expectedRpc = SafeDataOutputStream.serialize(LargeOracleRpc.createDispute(account1, id));
    assertThat(actualRpc).isEqualTo(expectedRpc);
  }

  @Test
  public void addCounterClaimsToDispute() {
    long nonce = 101L;
    Dispute.Transaction transaction = new Dispute.Transaction(receiver, amount, 0b111);
    Dispute.Transaction counterClaim =
        new Dispute.Transaction(receiver2, amount.add(Unsigned256.ONE), 0b111);
    Dispute dispute = Dispute.create(account1, transaction);
    WithdrawId withdrawId = new WithdrawId(0, nonce);
    state = state.createDispute(withdrawId, dispute);

    from(largeOracleContract);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(ByocOutgoingContract.Invocations.DISPUTE_COUNTER_CLAIM);
              rpc.writeLong(0);
              rpc.writeLong(nonce);
              counterClaim.write(rpc);
            });

    // These counter-claims are duplicates and should not be added
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(ByocOutgoingContract.Invocations.DISPUTE_COUNTER_CLAIM);
              rpc.writeLong(0);
              rpc.writeLong(nonce);
              counterClaim.write(rpc);
            });

    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(ByocOutgoingContract.Invocations.DISPUTE_COUNTER_CLAIM);
              rpc.writeLong(0);
              rpc.writeLong(nonce);
              transaction.write(rpc);
            });

    Dispute activeDispute =
        state.getDispute(withdrawId.oracleNonce(), withdrawId.withdrawalNonce());
    assertThat(activeDispute.getClaims()).hasSize(2);
    assertThat(activeDispute.getClaims().get(0)).isEqualTo(transaction);
    assertThat(activeDispute.getClaims().get(1)).isEqualTo(counterClaim);
  }

  @Test
  public void addCounterClaimFailsIfDisputeIsNotPresent() {
    long nonce = 101L;
    Dispute.Transaction transaction = new Dispute.Transaction(receiver, amount, 0b111);
    Dispute.Transaction counterClaim =
        new Dispute.Transaction(receiver, amount.add(Unsigned256.ONE), 0b111);
    Dispute dispute = Dispute.create(account1, transaction);
    state = state.createDispute(new WithdrawId(0, nonce), dispute);
    from(largeOracleContract);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(ByocOutgoingContract.Invocations.DISPUTE_COUNTER_CLAIM);
                      rpc.writeLong(0);
                      rpc.writeLong(102);
                      counterClaim.write(rpc);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("A dispute for withdrawal 102 and oracle 0 does not exist");
  }

  @Test
  public void counterClaimsShouldBeSentThroughLargeOracle() {
    from(account2);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(ByocOutgoingContract.Invocations.DISPUTE_COUNTER_CLAIM);
                      rpc.writeLong(0);
                      rpc.writeLong(0);
                      new Dispute.Transaction(receiver, Unsigned256.ONE, 0b111).write(rpc);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Counter-claim can only be sent through the large oracle contract");
  }

  @Test
  public void archivedDisputesAreCheckedBeforeAddingNew() {
    state =
        state
            .createPendingWithdrawal(requestingTransaction, receiver2, amount)
            .createPendingWithdrawal(requestingTransaction, receiver, amount.add(Unsigned256.ONE))
            .createPendingWithdrawal(
                requestingTransaction, receiver2, amount.subtract(Unsigned256.ONE))
            .createPendingWithdrawal(requestingTransaction, receiver, amount);
    from(account2);
    sendAndArchiveDispute(0, receiver, amount);
    sendAndArchiveDispute(1, receiver, amount);
    sendAndArchiveDispute(2, receiver2, amount);
    sendAndArchiveDispute(3, receiver2, amount.add(Unsigned256.ONE));
    trySendDispute(0, receiver, amount);
    trySendDispute(1, receiver, amount);
    trySendDispute(2, receiver2, amount);
    trySendDispute(3, receiver2, amount.add(Unsigned256.ONE));
  }

  @Test
  public void claimThatHasAsNotFraudCannotBeDisputedAgain() {
    long nonce = 101L;
    Dispute.Transaction transaction = new Dispute.Transaction(receiver, amount, 0b111);
    WithdrawId withdrawId = new WithdrawId(0, nonce);
    Dispute dispute = Dispute.create(account1, transaction);
    state = state.createDispute(withdrawId, dispute);
    state = state.archiveActiveDispute(withdrawId, ByocOutgoingContract.DISPUTE_RESULT_FALSE_CLAIM);

    trySendDispute(nonce, receiver, amount);
  }

  @Test
  public void disputeThatWasDroppedCanBeDisputedAgain() {
    long nonce = 0L;
    Dispute.Transaction transaction = new Dispute.Transaction(receiver, amount, 0b111);
    WithdrawId id = new WithdrawId(0, nonce);
    Dispute dispute = Dispute.create(account1, transaction);
    state = state.createDispute(id, dispute);
    state = state.dropDispute(id.oracleNonce(), id.withdrawalNonce());

    from(account2);
    sendAndArchiveDispute(nonce, receiver, amount);
  }

  private void sendAndArchiveDispute(long nonce, EthereumAddress receiver, Unsigned256 amount) {
    Dispute.Transaction transaction = new Dispute.Transaction(receiver, amount, 0b111);
    WithdrawId id = new WithdrawId(0, nonce);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(ByocOutgoingContract.Invocations.DISPUTE_CREATE);
              rpc.writeLong(0);
              rpc.writeLong(nonce);
              transaction.write(rpc);
            });
    state = state.archiveActiveDispute(id, 0);
  }

  private void trySendDispute(long nonce, EthereumAddress receiver, Unsigned256 amount) {
    Dispute.Transaction transaction = new Dispute.Transaction(receiver, amount, 0b111);
    from(account1);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(ByocOutgoingContract.Invocations.DISPUTE_CREATE);
                      rpc.writeLong(0);
                      rpc.writeLong(nonce);
                      transaction.write(rpc);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Withdrawal has already been disputed");
  }

  @Test
  public void disputeResultInsufficientStakes() {
    long nonce = 101L;
    Dispute.Transaction transaction = new Dispute.Transaction(receiver, amount, 0b111);
    Dispute dispute = Dispute.create(account1, transaction);
    state = state.createDispute(new WithdrawId(0, nonce), dispute);

    assertThat(state.getDispute(0, nonce)).isNotNull();

    sendDisputeResult(nonce, ByocOutgoingContract.DISPUTE_RESULT_INSUFFICIENT_TOKENS);

    assertThat(state.getDispute(0, nonce)).isNull();
  }

  @Test
  public void disputeResultFalseClaim() {
    long nonce = 101L;
    Dispute.Transaction transaction = new Dispute.Transaction(receiver, amount, 0b111);
    Dispute dispute = Dispute.create(account1, transaction);
    WithdrawId withdrawId = new WithdrawId(0, nonce);
    state = state.createDispute(withdrawId, dispute);

    sendDisputeResult(nonce, ByocOutgoingContract.DISPUTE_RESULT_FALSE_CLAIM);

    Dispute archivedDispute =
        state.getDispute(withdrawId.oracleNonce(), withdrawId.withdrawalNonce());
    assertThat(archivedDispute.getVotingResult())
        .isEqualTo(ByocOutgoingContract.DISPUTE_RESULT_FALSE_CLAIM);
  }

  @Test
  public void disputeResultFraudulentTransaction() {
    state = state.createPendingWithdrawal(requestingTransaction, receiver2, Unsigned256.ZERO);
    long nonce = 0L;
    Dispute.Transaction transaction = new Dispute.Transaction(receiver, amount, 0b111);
    Dispute dispute = Dispute.create(account1, transaction); // Type 2 fraud
    WithdrawId withdrawId = new WithdrawId(0, nonce);
    state = state.createDispute(withdrawId, dispute);

    sendDisputeResult(nonce, 0);

    // Assert that dispute has been archived
    Dispute archivedDispute =
        state.getDispute(withdrawId.oracleNonce(), withdrawId.withdrawalNonce());
    assertThat(archivedDispute.getVotingResult()).isEqualTo(0);
    byte[] serializedWithdrawal =
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeLong(0L);
              stream.write(receiver2.getIdentifier());
              Unsigned256.ZERO.write(stream);
            });
    Hash merkleTree = MerkleTree.buildTreeFrom(List.of(serializedWithdrawal));
    // Assert that a new small oracle has been requested
    ContractEventInteraction eventInteraction =
        (ContractEventInteraction) context.getInteractions().get(0);
    byte[] actualRpc = SafeDataOutputStream.serialize(eventInteraction.rpc);
    byte[] smallOracleContext =
        SafeDataOutputStream.serialize(
            stream -> {
              stream.write(byocContract.getIdentifier());
              stream.writeLong(0L);
              merkleTree.write(stream);
            });
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(LargeOracleRpc.requestNewSmallOracle(smallOracleContext));
    assertThat(actualRpc).isEqualTo(expectedRpc);

    // Assert that a message to burn oracles tokens has been sent
    eventInteraction = (ContractEventInteraction) context.getInteractions().get(1);
    actualRpc = SafeDataOutputStream.serialize(eventInteraction.rpc);
    expectedRpc = SafeDataOutputStream.serialize(LargeOracleRpc.burnTokens(oracles));
    assertThat(actualRpc).isEqualTo(expectedRpc);
  }

  private void sendDisputeResult(long nonce, int result) {
    from(largeOracleContract);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(ByocOutgoingContract.Invocations.DISPUTE_RESULT);
              rpc.writeLong(0);
              rpc.writeLong(nonce);
              rpc.writeInt(result);
            });
  }

  @Test
  public void disputeMustExistForResult() {
    from(largeOracleContract);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(ByocOutgoingContract.Invocations.DISPUTE_RESULT);
                      rpc.writeLong(0);
                      rpc.writeLong(127);
                      rpc.writeInt(0);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("No active dispute found for withdrawal 127");
  }

  @Test
  public void onlyLargeOracleCanCallDisputeResult() {
    long nonce = 101L;
    Dispute.Transaction transaction = new Dispute.Transaction(receiver, amount, 0b111);
    Dispute dispute = Dispute.create(account1, transaction);
    state = state.createDispute(new WithdrawId(0, nonce), dispute);

    from(account2);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(ByocOutgoingContract.Invocations.DISPUTE_RESULT);
                      rpc.writeLong(0);
                      rpc.writeLong(0);
                      rpc.writeInt(0);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Dispute result can only be sent by the large oracle contract");
  }

  private void assertRecalibrate(
      byte[] actualRpc, Unsigned256 amount, List<BlockchainAddress> oracles) {
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(
            LargeOracleRpc.recalibrateTokens(state.getSymbol(), amount, oracles));
    assertThat(actualRpc).isEqualTo(expectedRpc);
  }

  @Test
  public void handleType2Fraud() {
    long nonce = 0L;
    state = state.createPendingWithdrawal(requestingTransaction, receiver, amount);
    assertType2FraudHandled(nonce, amount.add(Unsigned256.create(500_000)), 0b111, oracles, true);
  }

  @Test
  public void handleFraudWithNoCorrespondingPbcWithdrawal() {
    long nonce = 101L;
    assertType2FraudHandled(nonce, amount, 0b111, oracles, true);
  }

  @Test
  public void onlyResponsibleOraclesLoseTokensWhenCheating() {
    assertType2FraudHandled(0, amount, 0b011, List.of(oracle0, oracle1), true);
    assertType2FraudHandled(1, amount, 0b101, List.of(oracle0, oracle2), false);
    assertType2FraudHandled(2, amount, 0b110, List.of(oracle1, oracle2), false);
  }

  @Test
  public void onlyThreeLeastSignificantBitsInBitmaskIsChecked() {
    assertType2FraudHandled(0, amount, 0b1001, List.of(oracle0), true);
  }

  private void assertType2FraudHandled(
      long nonce,
      Unsigned256 fraudAmount,
      int signerBitmask,
      List<BlockchainAddress> responsibleOracles,
      boolean requestNewOracle) {
    Dispute.Transaction transaction = new Dispute.Transaction(receiver, fraudAmount, signerBitmask);
    Dispute dispute = Dispute.create(account1, transaction);
    WithdrawId id = new WithdrawId(0, nonce);
    state = state.createDispute(id, dispute);
    sendDisputeResult(nonce, 0);

    // Assert that a new small oracle has been requested
    ContractEventInteraction eventInteraction =
        (ContractEventInteraction) context.getInteractions().get(0);
    if (requestNewOracle) {
      byte[] actualRequestRpc = SafeDataOutputStream.serialize(eventInteraction.rpc);
      byte[] smallOracleContext =
          SafeDataOutputStream.serialize(
              stream -> {
                byocContract.write(stream);
                stream.writeLong(0);
                state.getEpochs().getValue(0L).getMerkleTree().write(stream);
              });
      byte[] expectedRequestRpc =
          SafeDataOutputStream.serialize(LargeOracleRpc.requestNewSmallOracle(smallOracleContext));
      assertThat(actualRequestRpc).isEqualTo(expectedRequestRpc);

      eventInteraction = (ContractEventInteraction) context.getInteractions().get(1);
    }
    byte[] actualBurnRpc = SafeDataOutputStream.serialize(eventInteraction.rpc);
    byte[] expectedBurnRpc =
        SafeDataOutputStream.serialize(LargeOracleRpc.burnTokens(responsibleOracles));
    assertThat(actualBurnRpc).isEqualTo(expectedBurnRpc);
    int nextPayload;
    if (requestNewOracle) {
      nextPayload = 2;
    } else {
      nextPayload = 1;
    }
    eventInteraction = (ContractEventInteraction) context.getInteractions().get(nextPayload);
    byte[] lastActualRpc = SafeDataOutputStream.serialize(eventInteraction.rpc);
    assertRecalibrate(lastActualRpc, fraudAmount, responsibleOracles);
  }

  @Test
  public void oracleFromPreviousEpochIsPunished() {
    BlockchainPublicKey oracle3Key = new KeyPair().getPublic();
    BlockchainPublicKey oracle4Key = new KeyPair().getPublic();
    OracleMember oracle3 = new OracleMember(oracle3Key.createAddress(), oracle3Key);
    OracleMember oracle4 = new OracleMember(oracle4Key.createAddress(), oracle4Key);

    List<OracleMember> oracles2 =
        List.of(
            oracle4, new OracleMember(oracle0, oracleKey0), new OracleMember(oracle1, oracleKey1));
    state =
        state
            .createPendingWithdrawal(requestingTransaction, receiver2, amount)
            .createPendingWithdrawal(requestingTransaction, receiver, Unsigned256.create(50))
            .endCurrentEpoch()
            .startNewEpoch(oracles2, 0)
            .createPendingWithdrawal(
                requestingTransaction, receiver, amount.multiply(Unsigned256.create(2)))
            .createPendingWithdrawal(requestingTransaction, receiver3, Unsigned256.create(5))
            .createPendingWithdrawal(requestingTransaction, receiver2, amount.add(Unsigned256.ONE))
            .createPendingWithdrawal(
                requestingTransaction, receiver, amount.subtract(Unsigned256.ONE))
            .endCurrentEpoch()
            .startNewEpoch(List.of(oracle4, oracle3, new OracleMember(oracle0, oracleKey0)), 0);

    long oracleNonce = 1;
    Dispute.Transaction transaction = new Dispute.Transaction(receiver3, amount, 0b111);
    Dispute dispute = Dispute.create(account2, transaction);
    state = state.createDispute(new WithdrawId(oracleNonce, 3), dispute);

    from(largeOracleContract);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(ByocOutgoingContract.Invocations.DISPUTE_RESULT);
              rpc.writeLong(1L);
              rpc.writeLong(3L);
              rpc.writeInt(0);
            });

    List<BlockchainAddress> identities = oracles2.stream().map(OracleMember::getIdentity).toList();
    ContractEventInteraction eventInteraction =
        (ContractEventInteraction) context.getInteractions().get(0);
    byte[] actualRpc = SafeDataOutputStream.serialize(eventInteraction.rpc);
    byte[] expectedRpc = SafeDataOutputStream.serialize(LargeOracleRpc.burnTokens(identities));
    assertThat(actualRpc).isEqualTo(expectedRpc);

    eventInteraction = (ContractEventInteraction) context.getInteractions().get(1);
    actualRpc = SafeDataOutputStream.serialize(eventInteraction.rpc);
    assertRecalibrate(actualRpc, amount, identities);
  }

  @Test
  public void updateSmallOracle() {
    assertThat(state.getEpochs()).extracting(AvlTree::size).isEqualTo(1);
    Epoch epoch = state.getEpochs().getValue(state.getCurrentEpoch());
    assertThat(epoch.getOracles()).isEqualTo(oracles);

    state =
        state.createPendingWithdrawal(requestingTransaction, receiver, amount).endCurrentEpoch();

    List<BlockchainAddress> newOracles = invokeUpdateSmallOracle();

    assertThat(context.getByocFeesGas()).isEqualTo(tax(amount));
    assertThat(context.getByocSymbol()).isEqualTo(symbol);
    assertThat(context.getByocNodes().size()).isEqualTo(FixedList.create().size());

    assertThat(state.getOracleNonce()).isEqualTo(1L);
    assertThat(state.getEpochs()).extracting(AvlTree::size).isEqualTo(2);
    assertThat(state.getEpochs().getValue(state.getCurrentEpoch()).getOracles())
        .isEqualTo(newOracles);

    Epoch endedEpoch = state.getEpochs().getValue(0L);
    assertThat(endedEpoch.getMerkleTree()).isNotNull();
  }

  @Test
  public void updateSmallOracleWithMultipleRequests() {
    state =
        state
            .createPendingWithdrawal(requestingTransaction, receiver, amount)
            .createPendingWithdrawal(requestingTransaction, receiver, amount)
            .endCurrentEpoch()
            .createPendingWithdrawal(requestingTransaction, receiver, amount)
            .createPendingWithdrawal(requestingTransaction, receiver, amount)
            .endCurrentEpoch();

    assertThat(state.getWithdrawNonce()).isEqualTo(0L);
    assertThat(state.getWithdrawalSum()).isEqualTo(Unsigned256.ZERO);

    List<BlockchainAddress> newOracles = invokeUpdateSmallOracle();
    ContractEventInteraction eventInteraction =
        (ContractEventInteraction) context.getInteractions().get(0);
    byte[] actualRpc = SafeDataOutputStream.serialize(eventInteraction.rpc);
    byte[] smallOracleContext =
        SafeDataOutputStream.serialize(
            stream -> {
              state.getByocContract().write(stream);
              stream.writeLong(1);
              state.getEpochs().getValue(0L).getMerkleTree().write(stream);
            });
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(LargeOracleRpc.requestNewSmallOracle(smallOracleContext));
    assertThat(actualRpc).isEqualTo(expectedRpc);

    assertThat(state.getOracleNonce()).isEqualTo(1L);
    assertThat(state.getEpochs()).extracting(AvlTree::size).isEqualTo(3);
    assertThat(state.getEpochs().getValue(1L)).extracting(Epoch::getOracles).isEqualTo(newOracles);
    assertThat(state.hasPendingUpdate()).isTrue();
  }

  @Test
  public void updateSmallOracleWithPreviousEpoch() {
    List<OracleMember> oracleMembers =
        List.of(
            new OracleMember(oracle0, oracleKey0),
            new OracleMember(oracle1, oracleKey1),
            new OracleMember(oracle2, oracleKey2));
    state =
        state
            .createPendingWithdrawal(requestingTransaction, receiver, amount)
            .createPendingWithdrawal(requestingTransaction, receiver, amount)
            .endCurrentEpoch()
            .startNewEpoch(oracleMembers, 0)
            .createPendingWithdrawal(requestingTransaction, receiver, amount)
            .createPendingWithdrawal(requestingTransaction, receiver, amount)
            .createPendingWithdrawal(requestingTransaction, receiver, amount)
            .createPendingWithdrawal(requestingTransaction, receiver, amount)
            .endCurrentEpoch();

    List<BlockchainAddress> newOracles = invokeUpdateSmallOracle();

    assertThat(state.getOracleNonce()).isEqualTo(2L);
    assertThat(state.getEpochs()).extracting(AvlTree::size).isEqualTo(3);
    assertThat(state.getOracleNodes()).isEqualTo(newOracles);
    assertThat(state.hasPendingUpdate()).isFalse();

    Epoch epoch = state.getEpochs().getValue(1L);
    assertThat(epoch.getOracles()).isEqualTo(oracles);
  }

  @Test
  public void addSignatureForPreviousEpoch() {
    KeyPair newOracle = new KeyPair();
    List<OracleMember> oracleMembers =
        List.of(
            new OracleMember(newOracle.getPublic().createAddress(), newOracle.getPublic()),
            new OracleMember(oracle1, oracleKey1),
            new OracleMember(oracle2, oracleKey2));
    state =
        state
            .createPendingWithdrawal(requestingTransaction, receiver, amount)
            .createPendingWithdrawal(requestingTransaction, receiver, amount)
            .endCurrentEpoch()
            .startNewEpoch(oracleMembers, 0);

    addSignature(oracle0, oracleKeyPair0, 0);
  }

  @Test
  public void requestSmallOracleUpdate() {
    state =
        state
            .createPendingWithdrawal(requestingTransaction, receiver, amount)
            .createPendingWithdrawal(requestingTransaction, receiver2, amount)
            .createPendingWithdrawal(requestingTransaction, receiver3, amount)
            .createPendingWithdrawal(requestingTransaction, receiver3, amount.add(Unsigned256.ONE))
            .createPendingWithdrawal(
                requestingTransaction, receiver2, amount.subtract(Unsigned256.ONE));
    state = state.endCurrentEpoch();
    assertThat(state.getOracleMembers()).isEmpty();
    ByocOutgoingContract.requestSmallOracleUpdate(
        context.getInvocationCreator().invoke(state.getLargeOracleContract()), state);

    ContractEventInteraction eventInteraction =
        (ContractEventInteraction) context.getInteractions().get(0);
    byte[] actualRpc = SafeDataOutputStream.serialize(eventInteraction.rpc);
    byte[] smallOracleContext =
        SafeDataOutputStream.serialize(
            stream -> {
              stream.write(byocContract.getIdentifier());
              stream.writeLong(0L);
              handBuildMerkleTreeRoot().write(stream);
            });
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(LargeOracleRpc.requestNewSmallOracle(smallOracleContext));
    assertThat(actualRpc).isEqualTo(expectedRpc);
  }

  @Test
  public void multipleRequestsForNewUpdates() {
    CallbackContext callbackContext = CallbackContext.create(FixedList.create(List.of()));

    state = state.createPendingWithdrawal(requestingTransaction, receiver, totalWithdrawalMaximum);

    state =
        state.createPendingWithdrawal(
            requestingTransaction, receiver, totalWithdrawalMaximum.subtract(Unsigned256.ONE));
    state =
        serialization.callback(
            context,
            callbackContext,
            state,
            stream -> {
              stream.writeByte(ByocOutgoingContract.Callbacks.CREATE_WITHDRAWAL);
              account1.write(stream);
              receiver.write(stream);
              Unsigned256.ONE.write(stream);
            });
    assertThat(context.getInteractions().size()).isEqualTo(1);

    state =
        state.createPendingWithdrawal(
            requestingTransaction, receiver, totalWithdrawalMaximum.subtract(Unsigned256.ONE));
    state =
        serialization.callback(
            context,
            callbackContext,
            state,
            stream -> {
              stream.writeByte(ByocOutgoingContract.Callbacks.CREATE_WITHDRAWAL);
              account1.write(stream);
              receiver.write(stream);
              Unsigned256.ONE.write(stream);
            });
    assertThat(context.getInteractions().size()).isEqualTo(1);
  }

  private Hash handBuildMerkleTreeRoot() {
    Hash withdrawal1Digest = // 6fa84a4bd2e39771b876a097bc200726498cc0cbea4ead74cace0a6b41a1f91c
        Hash.create(
            stream -> {
              stream.writeLong(0L);
              stream.write(receiver.getIdentifier());
              withoutTax(amount).write(stream);
            });

    Hash withdrawal2Digest = // 2dff7444d023b639f06f20b88b678faa0f6d62d32343c9f1d8d2f3e4c0365ad1
        Hash.create(
            stream -> {
              stream.writeLong(1L);
              stream.write(receiver2.getIdentifier());
              withoutTax(amount).write(stream);
            });

    Hash withdrawal3Digest = // 0ef56b99e389adfe5076365c05223973d965ef01c182b1beca15a4ad58505a63
        Hash.create(
            stream -> {
              stream.writeLong(2L);
              stream.write(receiver3.getIdentifier());
              withoutTax(amount).write(stream);
            });

    Hash withdrawal4Digest = // e41e27e7da16b996802e93ceffd27e0d02bc1abcc59bff61f4e6c09e5bd15d80
        Hash.create(
            stream -> {
              stream.writeLong(3L);
              stream.write(receiver3.getIdentifier());
              withoutTax(amount.add(Unsigned256.ONE)).write(stream);
            });

    Hash withdrawal5Digest = // 1be6bccbe693536d3fa41dcc6d99f75befed6740d2cd1dd420640b76506e9757
        Hash.create(
            stream -> {
              stream.writeLong(4L);
              stream.write(receiver2.getIdentifier());
              withoutTax(amount.subtract(Unsigned256.ONE)).write(stream);
            });

    Hash level2Digest1 = // c50666739c5ec24e13631a3af18a7c63717cb1797abbe74f9e725ed632d7d0c2
        Hash.create(
            stream -> {
              withdrawal2Digest.write(stream);
              withdrawal1Digest.write(stream);
            });

    Hash level2Digest2 = // b8c6886e1dcb5d5d0d1d75979e08cd20c62e4332dcc8b0c155d2e72b54a6ef4e
        Hash.create(
            stream -> {
              withdrawal3Digest.write(stream);
              withdrawal4Digest.write(stream);
            });

    Hash level3Digest1 = // c31826a2077fba808c9f54aa5aeff5d13ed429d8bcdb2d2465fe56806c83cc4c
        Hash.create(
            stream -> {
              level2Digest2.write(stream);
              level2Digest1.write(stream);
            });

    return Hash.create(
        stream -> {
          withdrawal5Digest.write(stream);
          level3Digest1.write(stream);
        });
  }

  private Unsigned256 withoutTax(Unsigned256 amount) {
    return amount.subtract(tax(amount));
  }

  private List<BlockchainAddress> invokeUpdateSmallOracle() {
    BlockchainAddress oracle3 =
        BlockchainAddress.fromString("000000000000000000000000000000000000004444");
    BlockchainPublicKey oracleKey3 = new KeyPair().getPublic();
    List<BlockchainAddress> oracleIdentities = List.of(oracle2, oracle3, oracle0);
    List<BlockchainPublicKey> oraclePubKeys = List.of(oracleKey2, oracleKey3, oracleKey0);
    from(largeOracleContract);
    state =
        serialization.invoke(
            context,
            state,
            stream -> {
              stream.writeByte(ByocOutgoingContract.Invocations.UPDATE_SMALL_ORACLE);
              BlockchainAddress.LIST_SERIALIZER.writeDynamic(stream, oracleIdentities);
              BlockchainPublicKey.LIST_SERIALIZER.writeDynamic(stream, oraclePubKeys);
            });
    return List.of(oracle2, oracle3, oracle0);
  }

  @Test
  public void updateMustComeFromLargeOracleContract() {
    from(oracle0);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    stream -> {
                      stream.writeByte(ByocOutgoingContract.Invocations.UPDATE_SMALL_ORACLE);
                      BlockchainAddress.LIST_SERIALIZER.writeDynamic(stream, List.of());
                      BlockchainPublicKey.LIST_SERIALIZER.writeDynamic(stream, List.of());
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Updates to the small oracle can only be done through the large oracle contract");
  }

  @Test
  public void upgradeFromMinimalInitialState() {
    List<OracleMember> oracleMembers =
        List.of(
            new OracleMember(oracle0, oracleKey0),
            new OracleMember(oracle1, oracleKey1),
            new OracleMember(oracle2, oracleKey2));
    ByocOutgoingContractState initialState =
        new ByocOutgoingContractState(
            largeOracleContract,
            byocContract,
            symbol,
            oracleMembers,
            withdrawMinimum,
            totalWithdrawalMaximum,
            0);
    StateAccessor stateAccessor = StateAccessor.create(initialState);
    ByocOutgoingContract contract = new ByocOutgoingContract();
    ByocOutgoingContractState newState = contract.upgrade(stateAccessor);

    assertThat(newState.getLargeOracleContract()).isEqualTo(largeOracleContract);
    assertThat(newState.getByocContract().getIdentifier()).isEqualTo(byocContract.getIdentifier());
    assertThat(newState.getSymbol()).isEqualTo(symbol);
    assertThat(newState.getOracleNodes()).isEqualTo(oracles);
    assertThat(newState.getOracleMembers())
        .extracting(OracleMember::getKey)
        .isEqualTo(List.of(oracleKey0, oracleKey1, oracleKey2));
    assertThat(newState.getWithdrawNonce()).isEqualTo(0L);
    assertThat(newState.getWithdrawMinimum()).isEqualTo(withdrawMinimum);
    assertThat(newState.getMaximumWithdrawalPerEpoch()).isEqualTo(totalWithdrawalMaximum);
  }

  @Test
  public void upgradeFromCurrentComplexState() {
    ByocOutgoingContractState complexState = createComplexState(state);
    StateAccessor stateAccessor = StateAccessor.create(complexState);

    ByocOutgoingContract contract = new ByocOutgoingContract();
    ByocOutgoingContractState newState = contract.upgrade(stateAccessor);

    assertComplexStateMigrated(newState, complexState);
  }

  private void assertComplexStateMigrated(
      ByocOutgoingContractState newState, ByocOutgoingContractState complexState) {
    assertThat(newState.getOracleNodes()).isEqualTo(complexState.getOracleNodes());
    assertThat(newState.getOracleMembers())
        .extracting(OracleMember::getKey)
        .isEqualTo(complexState.getOracleMembers().stream().map(OracleMember::getKey).toList());
    assertThat(newState.getWithdrawNonce()).isEqualTo(complexState.getWithdrawNonce());
    assertWithdrawalsMigrated(complexState, newState);
    assertThat(newState.getWithdrawalSum()).isEqualTo(complexState.getWithdrawalSum());
    assertThat(newState.getOracleNonce()).isEqualTo(complexState.getOracleNonce());
    assertEpochsMigrated(complexState, newState);
    assertThat(newState.getOracleTimestamp()).isEqualTo(complexState.getOracleTimestamp());
  }

  private void assertWithdrawalsMigrated(
      ByocOutgoingContractState oldState, ByocOutgoingContractState newState) {
    List<Withdrawal> newWithdrawals = newState.getWithdrawals().values();
    List<Withdrawal> oldWithdrawals = oldState.getWithdrawals().values();
    assertThat(newWithdrawals.stream().map(Withdrawal::getAmount).toList())
        .isEqualTo(oldWithdrawals.stream().map(Withdrawal::getAmount).toList());
    assertThat(newWithdrawals.stream().map(Withdrawal::getSignatures).toList())
        .isEqualTo(oldWithdrawals.stream().map(Withdrawal::getSignatures).toList());

    for (int i = 0; i < newWithdrawals.size(); i++) {
      Withdrawal migrated = newWithdrawals.get(i);
      Withdrawal old = oldWithdrawals.get(i);
      assertThat(migrated.getReceiver().getIdentifier())
          .isEqualTo(old.getReceiver().getIdentifier());
    }
  }

  private void assertEpochsMigrated(
      ByocOutgoingContractState oldState, ByocOutgoingContractState newState) {
    List<Epoch> newEpochs = newState.getEpochs().values();
    List<Epoch> oldEpochs = oldState.getEpochs().values();
    assertThat(
            newEpochs.stream()
                .map(Epoch::getWithdrawals)
                .map(AvlTree::values)
                .flatMap(List::stream)
                .toList())
        .usingRecursiveFieldByFieldElementComparator()
        .isEqualTo(
            oldEpochs.stream()
                .map(Epoch::getWithdrawals)
                .map(AvlTree::values)
                .flatMap(List::stream)
                .toList());
    assertThat(
            newEpochs.stream()
                .map(Epoch::getDisputes)
                .map(AvlTree::values)
                .flatMap(List::stream)
                .toList())
        .usingRecursiveFieldByFieldElementComparator()
        .isEqualTo(
            oldEpochs.stream()
                .map(Epoch::getDisputes)
                .map(AvlTree::values)
                .flatMap(List::stream)
                .toList());
    assertThat(newEpochs.stream().map(Epoch::getOracles).toList())
        .isEqualTo(oldEpochs.stream().map(Epoch::getOracles).toList());
    assertThat(newEpochs.stream().map(Epoch::getMerkleTree).toList())
        .isEqualTo(oldEpochs.stream().map(Epoch::getMerkleTree).toList());
  }

  private ByocOutgoingContractState createComplexState(ByocOutgoingContractState state) {
    ByocOutgoingContractState updatedState = state;
    for (int i = 0; i < 3; i++) {
      updatedState =
          updatedState
              .createPendingWithdrawal(
                  requestingTransaction, receiver, amount.subtract(Unsigned256.create(i)))
              .addSignature(oracle0, 0, 0, randomSignature())
              .addSignature(oracle1, 0, 0, randomSignature())
              .addSignature(oracle2, 0, 0, randomSignature());
    }
    Dispute.Transaction transaction = new Dispute.Transaction(receiver3, amount, 0b111);
    Dispute dispute = Dispute.create(oracle0, transaction);
    WithdrawId id = new WithdrawId(0, 0);
    updatedState = updatedState.createDispute(id, dispute).archiveActiveDispute(id, 0);

    updatedState = updatedState.endCurrentEpoch();
    updatedState = updatedState.startNewEpoch(generateNewOracles(), 100);

    for (int i = 3; i < 8; i++) {
      updatedState =
          updatedState
              .createPendingWithdrawal(
                  requestingTransaction, receiver2, amount.subtract(Unsigned256.create(i)))
              .addSignature(oracle0, 0, 0, randomSignature())
              .addSignature(oracle1, 0, 0, randomSignature())
              .addSignature(oracle2, 0, 0, randomSignature());
    }

    transaction = new Dispute.Transaction(receiver3, amount, 0b111);
    dispute = Dispute.create(oracle0, transaction);
    id = new WithdrawId(1, 2);
    updatedState = updatedState.createDispute(id, dispute).archiveActiveDispute(id, 0);

    transaction = new Dispute.Transaction(receiver, amount, 0b111);
    dispute = Dispute.create(oracle0, transaction);
    id = new WithdrawId(1, 4);
    updatedState = updatedState.createDispute(id, dispute);

    updatedState = updatedState.endCurrentEpoch();

    return updatedState;
  }

  /** A governance contract can request a new oracle. */
  @Test
  public void requestNewSmallOracleAsVoting() {
    from(votingContract);
    assertThat(state.getCurrentEpoch()).isEqualTo(0);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(ByocOutgoingContract.Invocations.REQUEST_NEW_ORACLE);
            });
    // Ensure that oracle was replaced despite being less than 28 days old
    assertThat(state.getCurrentEpoch()).isEqualTo(1);
  }

  /** If one month has passed an oracle member can request a new oracle. */
  @Test
  public void requestNewOracle() {
    setBlockProductionTime(ByocOutgoingContract.ONE_MONTH_MILLISECONDS + 10);
    from(oracle1);
    ByocOutgoingContractState requestedNewOracle =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(ByocOutgoingContract.Invocations.REQUEST_NEW_ORACLE);
            });
    assertThat(requestedNewOracle.getOracleNodes()).isEmpty();

    byte[] smallOracleContext =
        SafeDataOutputStream.serialize(
            stream -> {
              state.getByocContract().write(stream);
              stream.writeLong(0);
              MerkleTree.buildTreeFrom(List.of()).write(stream);
            });
    ContractEventInteraction callbackInteraction =
        (ContractEventInteraction) context.getInteractions().get(0);
    byte[] actualRpc = SafeDataOutputStream.serialize(callbackInteraction.rpc);

    byte[] expectedRpc =
        SafeDataOutputStream.serialize(LargeOracleRpc.requestNewSmallOracle(smallOracleContext));
    assertThat(actualRpc).isEqualTo(expectedRpc);
    SafeDataInputStream input = SafeDataInputStream.createFromBytes(actualRpc);
    assertThat(input.readUnsignedByte()).isEqualTo(LargeOracleRpc.REQUEST_NEW_SMALL_ORACLE);
    assertThat(input.readLong()).isEqualTo(LargeOracleRpc.REQUIRED_ORACLE_STAKE);

    assertThat(input.readUnsignedByte())
        .isEqualTo(ByocOutgoingContract.Invocations.UPDATE_SMALL_ORACLE);
    assertThat(input.readDynamicBytes()).isEqualTo(smallOracleContext);

    from(largeOracleContract);
    ByocOutgoingContractState newOracles =
        serialization.invoke(
            context,
            requestedNewOracle,
            rpc -> {
              rpc.writeByte(ByocOutgoingContract.Invocations.UPDATE_SMALL_ORACLE);
              BlockchainAddress.LIST_SERIALIZER.writeDynamic(rpc, oracles);
              BlockchainPublicKey.LIST_SERIALIZER.writeDynamic(
                  rpc, List.of(oracleKey1, oracleKey2, oracleKey0));
            });
    assertThat(newOracles.getOracleTimestamp())
        .isEqualTo(ByocOutgoingContract.ONE_MONTH_MILLISECONDS + 10);
  }

  /**
   * If an oracle member requests a new oracle before one month has passed the contract will attempt
   * to replace non-BP oracle.
   */
  @Test
  public void requestNewOracleBeforeOneMonth() {
    from(oracle0);
    setBlockProductionTime(ByocOutgoingContract.ONE_MONTH_MILLISECONDS);
    serialization.invoke(
        context, state, rpc -> rpc.writeByte(ByocOutgoingContract.Invocations.REQUEST_NEW_ORACLE));

    byte[] actualRpc = SafeDataOutputStream.serialize(getRemoteCalls().get(0).rpc::write);
    byte[] expectedRpc = SafeDataOutputStream.serialize(LargeOracleRpc.checkLoStatus(oracles));
    assertThat(actualRpc).isEqualTo(expectedRpc);
    assertThat(context.getRemoteCalls().callbackRpc[0])
        .isEqualTo((byte) ByocOutgoingContract.Callbacks.REPLACE_NON_BP_SMALL_ORACLE);
  }

  /**
   * If a non-oracle member requests a new oracle after one month has passed the contract will
   * attempt to replace non-BP oracle.
   */
  @Test
  public void requestNewOracleNotSmallOracleMemberCallAfterOneMonth() {
    setBlockProductionTime(ByocOutgoingContract.ONE_MONTH_MILLISECONDS + 1);
    serialization.invoke(
        context, state, rpc -> rpc.writeByte(ByocOutgoingContract.Invocations.REQUEST_NEW_ORACLE));
    byte[] actualRpc = SafeDataOutputStream.serialize(s -> getRemoteCalls().get(0).rpc.write(s));
    byte[] expectedRpc = SafeDataOutputStream.serialize(LargeOracleRpc.checkLoStatus(oracles));
    assertThat(actualRpc).isEqualTo(expectedRpc);
    assertThat(context.getRemoteCalls().callbackRpc[0])
        .isEqualTo((byte) ByocOutgoingContract.Callbacks.REPLACE_NON_BP_SMALL_ORACLE);
  }

  /** If small oracle contains non-BP member anyone can request a new small oracle. */
  @Test
  public void replaceNonBpOracle() {
    BlockchainAddress nonOracleAccount =
        BlockchainAddress.fromString("000000000000000000000000000000000123450011");
    from(nonOracleAccount);
    ByocOutgoingContractState previousState = state;
    ByocOutgoingContractState invokedState =
        serialization.invoke(
            context,
            state,
            rpc -> rpc.writeByte(ByocOutgoingContract.Invocations.REQUEST_NEW_ORACLE));
    assertThat(invokedState).usingRecursiveComparison().isEqualTo(previousState);
    byte[] actualRpc = SafeDataOutputStream.serialize(getRemoteCalls().get(0).rpc);
    byte[] expectedRpc = SafeDataOutputStream.serialize(LargeOracleRpc.checkLoStatus(oracles));
    assertThat(actualRpc).isEqualTo(expectedRpc);
    SafeDataInputStream input = SafeDataInputStream.createFromBytes(actualRpc);
    assertThat(input.readUnsignedByte()).isEqualTo(LargeOracleRpc.CHECK_ORACLE_MEMBER_STATUS);
    assertThat(input.readInt()).isEqualTo(oracles.size());
    assertThat(BlockchainAddress.read(input)).isEqualTo(oracle0);
    assertThat(BlockchainAddress.read(input)).isEqualTo(oracle1);
    assertThat(BlockchainAddress.read(input)).isEqualTo(oracle2);
  }

  /** If there is currently an ongoing oracle change a new oracle cannot be requested. */
  @Test
  public void requestOracleOngoingOracleChange() {
    from(oracle0);
    setBlockProductionTime(
        ByocOutgoingContract.ONE_MONTH_MILLISECONDS + state.getOracleTimestamp());
    ByocOutgoingContractState waitingForOracleState =
        serialization.invoke(
            context,
            state,
            rpc -> rpc.writeByte(ByocOutgoingContract.Invocations.REQUEST_NEW_ORACLE));
    assertThat(waitingForOracleState.getOracleNodes()).isEmpty();

    assertThatThrownBy(
        () ->
            serialization.invoke(
                context,
                waitingForOracleState,
                rpc -> rpc.writeByte(ByocOutgoingContract.Invocations.REQUEST_NEW_ORACLE)),
        "Cannot request new small oracle during small oracle change");
  }

  /** If small oracle is found to contain a non-BP oracle member a new small oracle is requested. */
  @Test
  public void replaceNonBpOracleCallbackChangeOracle() {
    CallbackContext callbackContext = checkLoStatusCallbackResult(false);
    callbackContext.setSuccess(true);
    ByocOutgoingContractState previousState = state;
    ByocOutgoingContractState updatedOracleState =
        serialization.callback(
            context,
            callbackContext,
            state,
            ByocOutgoingContract.Callbacks.replaceNonBpOracle(state.getOracleNonce()));
    assertThat(updatedOracleState.getOracleNodes()).isEmpty();
    assertThat(updatedOracleState.getOracleNonce()).isEqualTo(previousState.getOracleNonce());
    byte[] smallOracleContext =
        SafeDataOutputStream.serialize(
            stream -> {
              state.getByocContract().write(stream);
              stream.writeLong(0);
              MerkleTree.buildTreeFrom(List.of()).write(stream);
            });
    byte[] actualRpc = SafeDataOutputStream.serialize(getInteractions().get(0).rpc);
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(
            s -> LargeOracleRpc.requestNewSmallOracle(smallOracleContext).write(s));
    assertThat(actualRpc).isEqualTo(expectedRpc);
  }

  /**
   * If small oracle is found to contain only BP oracle members a new small oracle is not requested.
   */
  @Test
  public void replaceNonBpOracleCallbackWithAllBpOracle() {
    CallbackContext callbackContext = checkLoStatusCallbackResult(true);
    callbackContext.setSuccess(true);
    assertThatThrownBy(
            () ->
                serialization.callback(
                    context,
                    callbackContext,
                    state,
                    ByocOutgoingContract.Callbacks.replaceNonBpOracle(state.getOracleNonce())))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Cannot replace non-BP oracle as all oracle members of nonce 0 are block producers");
  }

  /** If oracle changed during callback a new small oracle is not requested. */
  @Test
  public void replaceNonBpOracleCallbackOracleNotSame() {
    CallbackContext callbackContext = checkLoStatusCallbackResult(true);
    callbackContext.setSuccess(true);
    long previousNonce = state.getOracleNonce() - 1;
    assertThatThrownBy(
            () ->
                serialization.callback(
                    context,
                    callbackContext,
                    state,
                    ByocOutgoingContract.Callbacks.replaceNonBpOracle(previousNonce)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Oracle with nonce " + previousNonce + " has already been changed");
  }

  /**
   * Create large oracle callback result for checking oracle status.
   *
   * @param membersAreLargeOracle true if all small oracle members are part of large oracle
   * @return callback with large oracle result
   */
  private static CallbackContext checkLoStatusCallbackResult(boolean membersAreLargeOracle) {
    byte[] allMembersIsPartOfLargeOracleRpc =
        SafeDataOutputStream.serialize(s -> s.writeBoolean(membersAreLargeOracle));
    SafeDataInputStream callbackCheckResult =
        SafeDataInputStream.createFromBytes(allMembersIsPartOfLargeOracleRpc);
    CallbackContext.ExecutionResult callbackResult =
        CallbackContext.createResult(null, true, callbackCheckResult);
    return CallbackContext.create(FixedList.create(List.of(callbackResult)));
  }

  private void setBlockProductionTime(long blockProductionTime) {
    this.context =
        new SysContractContextTest(blockProductionTime, context.getBlockTime(), context.getFrom());
  }

  private SafeDataInputStream emptyRpc() {
    return SafeDataInputStream.createFromBytes(new byte[0]);
  }

  private void from(BlockchainAddress fromAddress) {
    context =
        new SysContractContextTest(
            context.getBlockProductionTime(), context.getBlockTime(), fromAddress);
  }

  private static Unsigned256 tax(Unsigned256 value) {
    return value.multiply(Unsigned256.ONE).divide(Unsigned256.create(1000));
  }

  private List<ContractEventInteraction> getInteractions() {
    return context.getInteractions().stream()
        .map(event -> (ContractEventInteraction) event)
        .toList();
  }

  private List<ContractEventInteraction> getRemoteCalls() {
    return context.getRemoteCalls().contractEvents.stream()
        .map(event -> (ContractEventInteraction) event)
        .toList();
  }
}
