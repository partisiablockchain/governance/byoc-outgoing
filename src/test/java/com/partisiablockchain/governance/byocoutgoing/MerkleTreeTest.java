package com.partisiablockchain.governance.byocoutgoing;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataOutputStream;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Test. */
public final class MerkleTreeTest {

  private final EthereumAddress receiver =
      EthereumAddress.fromString("EA7BEEF000000000000000000000000000000000");
  private final long amount = 123_456_789L;

  @Test
  public void merkleTreeFromEmptyLeaves() {
    Hash root = MerkleTree.buildTreeFrom(Collections.emptyList());
    assertThat(root).isEqualTo(Hash.create(stream -> {}));
  }

  @Test
  public void merkleTreeFromSingleLeaf() {
    EthereumAddress receiver =
        EthereumAddress.fromString("0000000000000000000000000000000000000003");
    long amount = 123_456_789L;
    byte[] element =
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeLong(0L);
              receiver.write(stream);
              stream.writeLong(amount);
            });
    Hash merkleTree = MerkleTree.buildTreeFrom(List.of(element));
    Hash leafDigest = Hash.create(stream -> stream.write(element));
    assertThat(merkleTree).isEqualTo(leafDigest);
  }

  @Test
  public void failureWhenBuildingMerkleTreeWithEqualLeaves() {
    byte[] element =
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeLong(0L);
              stream.write(receiver.getIdentifier());
              stream.writeLong(amount);
            });
    assertThatThrownBy(() -> MerkleTree.buildTreeFrom(List.of(element, element)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Two element's digests were the same");
  }
}
