package com.partisiablockchain.governance.byocoutgoing;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;

/** A member of a byoc-outgoing / withdrawal oracle. */
@Immutable
public final class OracleMember implements StateSerializable {

  private final BlockchainAddress identity;
  private final BlockchainPublicKey key;

  @SuppressWarnings("unused")
  OracleMember() {
    this.identity = null;
    this.key = null;
  }

  /**
   * Default constructor for this data class.
   *
   * @param identity the identity of the oracle member
   * @param key the key of the oracle member
   */
  public OracleMember(BlockchainAddress identity, BlockchainPublicKey key) {
    this.identity = identity;
    this.key = key;
  }

  static OracleMember fromStateAccessor(StateAccessor accessor) {
    BlockchainAddress identity = accessor.get("identity").blockchainAddressValue();
    BlockchainPublicKey key = accessor.get("key").blockchainPublicKeyValue();
    return new OracleMember(identity, key);
  }

  /**
   * Get the identity of the oracle member.
   *
   * @return a {@link BlockchainAddress} corresponding the oracle member's public key
   */
  public BlockchainAddress getIdentity() {
    return identity;
  }

  /**
   * Get the public key of the oracle member.
   *
   * @return the oracle member's public key
   */
  public BlockchainPublicKey getKey() {
    return key;
  }
}
