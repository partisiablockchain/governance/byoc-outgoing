package com.partisiablockchain.governance.byocoutgoing;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataOutputStream;
import java.util.Arrays;
import java.util.Objects;
import org.bouncycastle.util.encoders.Hex;

/**
 * Represents an address on the Ethereum Blockchain. The address is the last 20 bytes of the
 * keccak-256 hash of the account's/contract's public key. There are no identifiers distinguishing
 * accounts from contracts.
 */
@Immutable
public record EthereumAddress(LargeByteArray identifier)
    implements DataStreamSerializable, StateSerializable {

  byte[] getIdentifier() {
    return identifier.getData();
  }

  static EthereumAddress fromString(String encodedAddress) {
    byte[] data = Hex.decode(encodedAddress);
    LargeByteArray identifier = new LargeByteArray(data);
    return new EthereumAddress(identifier);
  }

  static EthereumAddress fromStateAccessor(StateAccessor accessor) {
    LargeByteArray identifier = accessor.get("identifier").typedValue(LargeByteArray.class);
    return new EthereumAddress(identifier);
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    byte[] data = Objects.requireNonNull(identifier).getData();
    stream.write(data);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof EthereumAddress that)) {
      return false;
    }
    if (this.identifier == null || that.identifier == null) {
      return Objects.equals(this.identifier, that.identifier);
    }
    return Arrays.equals(this.identifier.getData(), that.identifier.getData());
  }

  @Override
  public int hashCode() {
    return Arrays.hashCode(identifier == null ? null : identifier.getData());
  }
}
