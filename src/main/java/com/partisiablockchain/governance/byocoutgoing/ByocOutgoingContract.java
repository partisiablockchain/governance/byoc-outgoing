package com.partisiablockchain.governance.byocoutgoing;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.FormatMethod;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.InteractionBuilder;
import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Callback;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.contract.sys.SystemEventManager;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** A system contract that facilitates outgoing BYOC transfers, i.e. withdrawals. */
@AutoSysContract(ByocOutgoingContractState.class)
public final class ByocOutgoingContract {

  private static final Logger logger = LoggerFactory.getLogger(ByocOutgoingContract.class);

  static final int BYOC_TAX_PERMIL = 1;
  static final int DISPUTE_RESULT_FALSE_CLAIM = -1;
  static final int DISPUTE_RESULT_INSUFFICIENT_TOKENS = -2;

  static final long ONE_MONTH_MILLISECONDS = Duration.ofDays(28).toMillis();

  static final class Invocations {

    static final int ADD_PENDING_WITHDRAWAL = 0;
    static final int SIGN_PENDING_WITHDRAWAL = 1;
    static final int DISPUTE_CREATE = 2;
    static final int DISPUTE_COUNTER_CLAIM = 3;
    static final int DISPUTE_RESULT = 4;
    static final int UPDATE_SMALL_ORACLE = 5;
    static final int REQUEST_NEW_ORACLE = 6;

    private Invocations() {}
  }

  static final class Callbacks {

    static final int REPLACE_NON_BP_SMALL_ORACLE = 2;
    static final int CREATE_WITHDRAWAL = 5;

    private Callbacks() {}

    static DataStreamSerializable createWithdrawal(
        BlockchainAddress sender, EthereumAddress receiver, Unsigned256 amount) {
      return data -> {
        data.writeByte(CREATE_WITHDRAWAL);
        sender.write(data);
        receiver.write(data);
        amount.write(data);
      };
    }

    static DataStreamSerializable replaceNonBpOracle(long oracleNonce) {
      return data -> {
        data.writeByte(REPLACE_NON_BP_SMALL_ORACLE);
        data.writeLong(oracleNonce);
      };
    }
  }

  /**
   * Initialize the outgoing contract.
   *
   * @param context execution context
   * @param largeOracleContract address of the large oracle
   * @param byocContract ethereum contract address
   * @param symbol of the BYOC twin
   * @param oracleIdentities identities of the oracles
   * @param oracleKeys oracle keys
   * @param withdrawMinimum minimum withdrawal amount
   * @param totalWithdrawalMaximum maximum withdrawal amount
   * @return initial state
   */
  @Init
  public ByocOutgoingContractState create(
      SysContractContext context,
      BlockchainAddress largeOracleContract,
      EthereumAddressRpc byocContract,
      String symbol,
      List<BlockchainAddress> oracleIdentities,
      List<BlockchainPublicKey> oracleKeys,
      Unsigned256 withdrawMinimum,
      Unsigned256 totalWithdrawalMaximum) {
    List<OracleMember> oracleMembers = createOracleMembers(oracleIdentities, oracleKeys);
    ByocOutgoingContractState state =
        new ByocOutgoingContractState(
            largeOracleContract,
            byocContract.convert(),
            symbol,
            oracleMembers,
            withdrawMinimum,
            totalWithdrawalMaximum,
            context.getBlockProductionTime());
    if (oracleMembers.isEmpty()) {
      // If contract is initialized with an empty oracle we need to proceed to the next epoch to get
      // a valid oracle that can be used on the partner chain. The partner chain has been
      // initialized with an empty oracle for the first epoch.
      InteractionBuilder invokeLargeOracle =
          context.getInvocationCreator().invoke(state.getLargeOracleContract());
      return endCurrentEpoch(invokeLargeOracle, state);
    } else {
      return state;
    }
  }

  /**
   * Migrate an existing contract to this version.
   *
   * @param oldState accessor for the old state
   * @return the migrated state
   */
  @Upgrade
  public ByocOutgoingContractState upgrade(StateAccessor oldState) {
    return ByocOutgoingContractState.migrateState(oldState);
  }

  /**
   * Request to withdraw an amount of BYOC. The amount requested must be smaller than the amount of
   * BYOC held by this account, larger than the withdrawal minimum, and less than the maximum
   * withdrawal per epoch.
   *
   * <p>If the requested amount does not exceed the withdrawal minimum or if it exceeds the maximum
   * withdrawal per epoch, then the invocation fails immediately. And if the requested amount
   * exceeds the current balance of the invoker, then the invocation fails in a callback.
   *
   * <p>If the request is valid, then a pending withdrawal request will be added when the callback
   * succeeds. This pending request will then be signed by the current small oracle for this BYOC
   * bridge.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param receiver the Ethereum address of the receiver
   * @param amount the amount withdrawn
   * @return updated state
   */
  @Action(Invocations.ADD_PENDING_WITHDRAWAL)
  public ByocOutgoingContractState addPendingWithdrawal(
      SysContractContext context,
      ByocOutgoingContractState state,
      EthereumAddressRpc receiver,
      Unsigned256 amount) {
    BlockchainAddress sender = context.getFrom();
    EthereumAddress ethereumAddress = receiver.convert();
    ensure(
        amount.compareTo(state.getWithdrawMinimum()) >= 0,
        "Amount must be larger than or equal the minimum: %s",
        state.getWithdrawMinimum());
    ensure(
        amount.compareTo(state.getMaximumWithdrawalPerEpoch()) <= 0,
        "Amount must be less than or equal to the maximum withdrawal per epoch: %s",
        state.getMaximumWithdrawalPerEpoch());

    SystemEventManager eventManager = context.getRemoteCallsCreator();
    eventManager.registerCallbackWithCostFromRemaining(
        Callbacks.createWithdrawal(sender, ethereumAddress, amount));

    LocalPluginStateUpdate accountUpdate =
        LocalPluginStateUpdate.create(
            sender, AccountPluginRpc.deductCoinBalance(state.getSymbol(), amount));
    eventManager.updateLocalAccountPluginState(accountUpdate);

    return state;
  }

  /**
   * Signs a pending withdraw previously added through {@link #addPendingWithdrawal}. Only members
   * of the small oracle associated with the withdraw request are allowed to add a signature.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param oracleNonce oracle nonce
   * @param withdrawalNonce the nonce associated with the withdraw request that was signed
   * @param signature the signature to add to the list of signatures for the withdrawal
   * @return updated state
   */
  @Action(Invocations.SIGN_PENDING_WITHDRAWAL)
  public ByocOutgoingContractState signPendingWithdrawal(
      SysContractContext context,
      ByocOutgoingContractState state,
      long oracleNonce,
      long withdrawalNonce,
      Signature signature) {

    Withdrawal withdrawal = state.getPendingWithdrawal(oracleNonce, withdrawalNonce);
    ensure(withdrawal != null, "Withdrawal with nonce %s does not exist", withdrawalNonce);
    BlockchainPublicKey senderPublicKey = getSenderPublicKey(context.getFrom(), state, oracleNonce);
    Hash messageHash = state.messageDigest(oracleNonce, withdrawalNonce, withdrawal);
    BlockchainPublicKey signer = signature.recoverPublicKey(messageHash);
    ensure(signer.equals(senderPublicKey), "Invalid signature");

    return state.addSignature(context.getFrom(), oracleNonce, withdrawalNonce, signature);
  }

  /**
   * Dispute a transaction.
   *
   * <p>A transaction can only be disputed if they:
   *
   * <ul>
   *   <li>Have no associated withdraw (i.e., something happened on the other chain that did not
   *       happen here)
   *   <li>Contains a mismatch with an existing withdraw (i.e., the withdraw requested on PBC does
   *       not match the withdraw executed on the other chain)
   * </ul>
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param withdrawId id of the withdrawal
   * @param transaction the transaction being disputed
   * @return updated state
   */
  @Action(Invocations.DISPUTE_CREATE)
  public ByocOutgoingContractState disputeCreate(
      SysContractContext context,
      ByocOutgoingContractState state,
      WithdrawId withdrawId,
      Dispute.TransactionRpc transaction) {
    Dispute.Transaction disputeTransaction = transaction.convert();
    Withdrawal withdrawal =
        state.getPendingWithdrawal(withdrawId.oracleNonce(), withdrawId.withdrawalNonce());
    boolean invalidDispute =
        withdrawal != null
            && Arrays.equals(
                withdrawal.getReceiver().getIdentifier(), disputeTransaction.getReceiver())
            && Objects.equals(withdrawal.getAmount(), disputeTransaction.amount());
    ensure(!invalidDispute, "Disputed transaction is equal to the withdrawal stored in state");

    BlockchainAddress challenger = context.getFrom();
    Dispute dispute = Dispute.create(challenger, disputeTransaction);

    // Send disputed transaction to large oracle for a vote.
    context
        .getInvocationCreator()
        .invoke(state.getLargeOracleContract())
        .withPayload(LargeOracleRpc.createDispute(challenger, withdrawId))
        .sendFromContract();

    return state.createDispute(withdrawId, dispute);
  }

  /**
   * Add a counter claim to a dispute. Only an existing dispute can be counter-claimed, and counter
   * claims can only be sent through the large oracle contract.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param withdrawId id of the withdrawal
   * @param transaction the transaction being disputed
   * @return updated state
   */
  @Action(Invocations.DISPUTE_COUNTER_CLAIM)
  public ByocOutgoingContractState disputeCounterClaim(
      SysContractContext context,
      ByocOutgoingContractState state,
      WithdrawId withdrawId,
      Dispute.TransactionRpc transaction) {
    Dispute.Transaction disputeTransaction = transaction.convert();
    BlockchainAddress largeOracle = state.getLargeOracleContract();
    BlockchainAddress sender = context.getFrom();
    ensure(
        largeOracle.equals(sender),
        "Counter-claim can only be sent through the large oracle contract");
    Dispute dispute = state.getDispute(withdrawId.oracleNonce(), withdrawId.withdrawalNonce());
    ensure(
        dispute != null,
        "A dispute for withdrawal %d and oracle %d does not exist",
        withdrawId.withdrawalNonce(),
        withdrawId.oracleNonce());

    return state.addCounterClaim(withdrawId, disputeTransaction);
  }

  /**
   * Resolve a dispute. A dispute resolution can only take place through the large oracle contract
   * (which will invoke this method).
   *
   * <p>A dispute is resolved when 2/3 of large oracle members have voted on the counter claims for
   * the dispute.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param withdrawId the unique identifier of the withdrawal
   * @param disputeResult the result of the dispute
   * @return updated state
   */
  @Action(Invocations.DISPUTE_RESULT)
  public ByocOutgoingContractState disputeResult(
      SysContractContext context,
      ByocOutgoingContractState state,
      WithdrawId withdrawId,
      int disputeResult) {
    BlockchainAddress largeOracle = state.getLargeOracleContract();
    BlockchainAddress sender = context.getFrom();
    ensure(
        largeOracle.equals(sender), "Dispute result can only be sent by the large oracle contract");

    Dispute dispute = state.getDispute(withdrawId.oracleNonce(), withdrawId.withdrawalNonce());
    ensure(
        dispute != null, "No active dispute found for withdrawal %d", withdrawId.withdrawalNonce());

    InteractionBuilder invokeLargeOracle = context.getInvocationCreator().invoke(largeOracle);

    if (disputeResult < 0) {
      if (disputeResult == DISPUTE_RESULT_INSUFFICIENT_TOKENS) {
        BlockchainAddress challenger = dispute.getChallenger();
        logger.info(
            "Challenger {} had insufficient tokens staked to large oracle to start a dispute",
            challenger);
        return state.dropDispute(withdrawId.oracleNonce(), withdrawId.withdrawalNonce());
      } else { // DISPUTE_RESULT_FALSE_CLAIM
        return state.archiveActiveDispute(withdrawId, disputeResult);
      }
    } else {
      Dispute.Transaction transaction = dispute.getClaims().get(disputeResult);
      return handleFraudulentClaim(state, withdrawId, transaction, invokeLargeOracle)
          .archiveActiveDispute(withdrawId, disputeResult);
    }
  }

  /**
   * Update the current small oracle.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param oracleIdentities the addresses of the new oracle
   * @param oraclePubKeys the public keys of the new oracle
   * @return updated state
   */
  @Action(Invocations.UPDATE_SMALL_ORACLE)
  public ByocOutgoingContractState updateSmallOracle(
      SysContractContext context,
      ByocOutgoingContractState state,
      List<BlockchainAddress> oracleIdentities,
      List<BlockchainPublicKey> oraclePubKeys) {
    BlockchainAddress from = context.getFrom();
    ensure(
        state.getLargeOracleContract().equals(from),
        "Updates to the small oracle can only be done through the large oracle contract");

    payOutCurrentEpoch(context, state);

    List<OracleMember> oracleMembers = createOracleMembers(oracleIdentities, oraclePubKeys);

    ByocOutgoingContractState updatedState =
        state.startNewEpoch(oracleMembers, context.getBlockProductionTime());

    if (updatedState.hasPendingUpdate()) {
      InteractionBuilder invokeLargeOracle =
          context.getInvocationCreator().invoke(updatedState.getLargeOracleContract());
      requestSmallOracleUpdate(invokeLargeOracle, updatedState);
    }
    return updatedState;
  }

  /**
   * Request selection of a new small oracle.
   *
   * <p>Criteria for oracle rotation:
   *
   * <ul>
   *   <li>Governance contracts can rotate the small oracle at any time.
   *   <li>Current members of the small oracle can rotate the small oracle if 28 days have passed
   *       since the current small oracle was started.
   *   <li>Anyone can rotate the small oracle if any member of the current oracle is not a block
   *       producer.
   *   <li>The small oracle cannot be rotated if a small oracle change is already in progress.
   * </ul>
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @return updated state if new oracle is requested
   */
  @Action(Invocations.REQUEST_NEW_ORACLE)
  public ByocOutgoingContractState requestNewOracle(
      SysContractContext context, ByocOutgoingContractState state) {
    BlockchainAddress sender = context.getFrom();
    ensure(
        !state.getOracleNodes().isEmpty(),
        "Cannot request new small oracle during small oracle change");
    long currentTime = context.getBlockProductionTime();
    boolean oneMonthHasPassed = currentTime - state.getOracleTimestamp() >= ONE_MONTH_MILLISECONDS;
    boolean senderIsOracleMember = state.getOracleNodes().contains(sender);
    if (sender.getType() == BlockchainAddress.Type.CONTRACT_GOV
        || (oneMonthHasPassed && senderIsOracleMember)) {
      return requestOracleChange(context, state);
    } else {
      checkLargeOracleStatusOfSmallOracleMembers(context, state);
      return state;
    }
  }

  /**
   * End current epoch and request a new small oracle.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @return state with new epoch
   */
  private static ByocOutgoingContractState requestOracleChange(
      SysContractContext context, ByocOutgoingContractState state) {
    ByocOutgoingContractState updatedState = state.endCurrentEpoch();
    InteractionBuilder invokeLargeOracle =
        context.getInvocationCreator().invoke(updatedState.getLargeOracleContract());
    requestSmallOracleUpdate(invokeLargeOracle, updatedState);
    return updatedState;
  }

  /**
   * Check with large oracle if all small oracle members are large oracle members. Requests a new
   * small oracle if any member is found not to be part of large oracle.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   */
  private static void checkLargeOracleStatusOfSmallOracleMembers(
      SysContractContext context, ByocOutgoingContractState state) {
    SystemEventManager eventManager = context.getRemoteCallsCreator();
    eventManager.registerCallbackWithCostFromRemaining(
        Callbacks.replaceNonBpOracle(state.getOracleNonce()));
    eventManager
        .invoke(state.getLargeOracleContract())
        .withPayload(LargeOracleRpc.checkLoStatus(state.getOracleNodes()))
        .sendFromContract();
  }

  /**
   * Request a new small oracle if at least one member is not part of large oracle.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param callbackContext information about callbacks
   * @param oracleNonce nonce of the oracle to be replaced
   * @return updated state
   */
  @Callback(Callbacks.REPLACE_NON_BP_SMALL_ORACLE)
  public ByocOutgoingContractState replaceNonBpOracleCallback(
      SysContractContext context,
      ByocOutgoingContractState state,
      CallbackContext callbackContext,
      Long oracleNonce) {
    boolean allMembersArePartOfLargeOracle =
        callbackContext.results().get(0).returnValue().readBoolean();
    boolean oracleHasNotChanged = state.getOracleNonce() == oracleNonce;
    ensure(oracleHasNotChanged, "Oracle with nonce %d has already been changed", oracleNonce);
    ensure(
        !allMembersArePartOfLargeOracle,
        "Cannot replace non-BP oracle as all oracle members of nonce %d are block producers",
        oracleNonce);

    return requestOracleChange(context, state);
  }

  void payOutCurrentEpoch(SysContractContext context, ByocOutgoingContractState state) {
    List<BlockchainAddress> currentOracleNodes = state.getOracleNodes(state.getOracleNonce());
    for (Withdrawal withdrawal : state.getWithdrawals().values()) {
      FixedList<BlockchainAddress> signers = withdrawal.getSigners(currentOracleNodes);
      context.registerDeductedByocFees(withdrawal.getTax(), state.getSymbol(), signers);
    }
  }

  private static BlockchainPublicKey getSenderPublicKey(
      BlockchainAddress senderIdentity, ByocOutgoingContractState state, long oracleNonce) {
    AvlTree<Long, Epoch> epochs = state.getEpochs();
    Epoch currentEpoch = epochs.getValue(oracleNonce);
    List<OracleMember> oracleMembers = currentEpoch.getOracleMembers();

    BlockchainPublicKey senderKey = null;
    for (OracleMember oracleMember : oracleMembers) {
      BlockchainAddress identity = oracleMember.getIdentity();
      if (identity.equals(senderIdentity)) {
        senderKey = oracleMember.getKey();
      }
    }
    ensure(senderKey != null, "Signature did not come from an oracle node");
    return senderKey;
  }

  private static ByocOutgoingContractState handleFraudulentClaim(
      ByocOutgoingContractState oldState,
      WithdrawId withdrawId,
      Dispute.Transaction transaction,
      InteractionBuilder invokeLargeOracle) {
    ByocOutgoingContractState updatedState = oldState;

    long oracleNonce = withdrawId.oracleNonce();
    if (oracleNonce == updatedState.getCurrentEpoch()) {
      updatedState = endCurrentEpoch(invokeLargeOracle, updatedState);
    }

    // Penalize responsible oracles for cheating
    List<BlockchainAddress> responsibleOracles =
        findResponsibleOracles(transaction, updatedState, oracleNonce);

    invokeLargeOracle.withPayload(LargeOracleRpc.burnTokens(responsibleOracles)).sendFromContract();

    invokeLargeOracle
        .withPayload(
            LargeOracleRpc.recalibrateTokens(
                oldState.getSymbol(), transaction.amount(), responsibleOracles))
        .sendFromContract();
    return updatedState;
  }

  private static List<BlockchainAddress> findResponsibleOracles(
      Dispute.Transaction transaction, ByocOutgoingContractState updatedState, long oracleNonce) {
    List<BlockchainAddress> oracles = updatedState.getEpochs().getValue(oracleNonce).getOracles();
    int bitmask = transaction.bitmask();
    List<BlockchainAddress> responsibleOracles = new ArrayList<>();
    for (int i = 0; i < oracles.size(); i++) {
      if ((bitmask >> i & 1) == 1) {
        responsibleOracles.add(oracles.get(i));
      }
    }
    return responsibleOracles;
  }

  /**
   * Checks if the withdrawal for the sender was successful and updated accordingly.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param callbackContext information about callbacks
   * @param sender sender of withdrawal
   * @param receiver receiver of the withdrawal
   * @param amount amount to be withdrawn
   * @return updated state
   */
  @Callback(Callbacks.CREATE_WITHDRAWAL)
  public ByocOutgoingContractState createWithdrawal(
      SysContractContext context,
      ByocOutgoingContractState state,
      CallbackContext callbackContext,
      BlockchainAddress sender,
      EthereumAddressRpc receiver,
      Unsigned256 amount) {
    ensure(
        callbackContext.isSuccess(),
        "Unable to withdraw %s %s from %s",
        amount,
        state.getSymbol(),
        sender);

    ByocOutgoingContractState updatedState = state;
    if (updatedState
            .getWithdrawalSum()
            .add(amount)
            .compareTo(updatedState.getMaximumWithdrawalPerEpoch())
        > 0) {
      InteractionBuilder invokeLargeOracle =
          context.getInvocationCreator().invoke(updatedState.getLargeOracleContract());
      updatedState = endCurrentEpoch(invokeLargeOracle, updatedState);
    }
    return updatedState.createPendingWithdrawal(
        context.getOriginalTransactionHash(), receiver.convert(), amount);
  }

  static ByocOutgoingContractState endCurrentEpoch(
      InteractionBuilder invokeLargeOracle, ByocOutgoingContractState state) {
    boolean pendingUpdate = state.hasPendingUpdate();
    ByocOutgoingContractState updatesState = state.endCurrentEpoch();
    if (!pendingUpdate) {
      requestSmallOracleUpdate(invokeLargeOracle, updatesState);
    }
    return updatesState;
  }

  static byte[] buildAdditionalData(ByocOutgoingContractState state) {
    long oracleNonce = state.getOracleNonce();
    Epoch oldEpoch = state.getEpochs().getValue(oracleNonce);
    Hash merkleTree = oldEpoch.getMerkleTree();
    return SafeDataOutputStream.serialize(
        stream -> {
          state.getByocContract().write(stream);
          stream.writeLong(oracleNonce);
          merkleTree.write(stream);
        });
  }

  static void requestSmallOracleUpdate(
      InteractionBuilder invokeLargeOracle, ByocOutgoingContractState state) {
    byte[] context = buildAdditionalData(state);
    invokeLargeOracle.withPayload(LargeOracleRpc.requestNewSmallOracle(context)).sendFromContract();
  }

  @FormatMethod
  private static void ensure(boolean predicate, String messageFormat, Object... args) {
    if (!predicate) {
      String errorMessage = String.format(messageFormat, args);
      throw new RuntimeException(errorMessage);
    }
  }

  private static List<OracleMember> createOracleMembers(
      List<BlockchainAddress> identities, List<BlockchainPublicKey> keys) {
    ensure(identities.size() == keys.size(), "Lengths of oracle identities and keys did not match");
    return IntStream.range(0, identities.size())
        .mapToObj(i -> new OracleMember(identities.get(i), keys.get(i)))
        .toList();
  }
}
