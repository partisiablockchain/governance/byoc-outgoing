package com.partisiablockchain.governance.byocoutgoing;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.google.errorprone.annotations.ImmutableTypeParameter;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateAccessorAvlLeafNode;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import java.util.List;
import java.util.function.Function;

/** State for outgoing BYOC transfer. */
@Immutable
public final class ByocOutgoingContractState implements StateSerializable {

  private final BlockchainAddress largeOracleContract;

  private final EthereumAddress byocContract;
  private final String symbol;

  private final long withdrawNonce;
  private final Unsigned256 withdrawMinimum;
  private final Unsigned256 withdrawalSum;
  private final Unsigned256 maximumWithdrawalPerEpoch;

  /** The currently active oracle as seen from large oracle contract. */
  private final long oracleNonce;

  /** The currently active epoch. */
  private final long currentEpoch;

  private final AvlTree<Long, Epoch> epochs;

  private final long oracleTimestamp;

  @SuppressWarnings("unused")
  ByocOutgoingContractState() {
    largeOracleContract = null;
    byocContract = null;
    symbol = null;
    withdrawNonce = 0L;
    withdrawMinimum = null;
    withdrawalSum = null;
    maximumWithdrawalPerEpoch = null;
    oracleNonce = 0L;
    currentEpoch = 0L;
    epochs = null;
    oracleTimestamp = 0L;
  }

  /**
   * Create a new contract state.
   *
   * @param largeOracleContract address of the large oracle contract
   * @param ethByocContract the ethereum address of the contract
   * @param symbol symbol of the BYOC twin
   * @param oracleMembers the currently active oracle members
   * @param withdrawMinimum the minimum amount to withdraw
   * @param maximumWithdrawalPerEpoch maximum amount of BYOC twins allowed to be withdrawn per epoch
   * @param oracleTimestamp timestamp of when the current oracle was enabled
   */
  public ByocOutgoingContractState(
      BlockchainAddress largeOracleContract,
      EthereumAddress ethByocContract,
      String symbol,
      List<OracleMember> oracleMembers,
      Unsigned256 withdrawMinimum,
      Unsigned256 maximumWithdrawalPerEpoch,
      long oracleTimestamp) {
    this(
        largeOracleContract,
        ethByocContract,
        symbol,
        0L,
        withdrawMinimum,
        Unsigned256.ZERO,
        maximumWithdrawalPerEpoch,
        0L,
        0L,
        AvlTree.<Long, Epoch>create().set(0L, Epoch.create().setOracles(oracleMembers)),
        oracleTimestamp);
  }

  private ByocOutgoingContractState(
      BlockchainAddress largeOracleContract,
      EthereumAddress byocContract,
      String symbol,
      long withdrawNonce,
      Unsigned256 withdrawMinimum,
      Unsigned256 withdrawalSum,
      Unsigned256 maximumWithdrawalPerEpoch,
      long oracleNonce,
      long currentEpoch,
      AvlTree<Long, Epoch> epochs,
      long oracleTimestamp) {
    this.largeOracleContract = largeOracleContract;
    this.byocContract = byocContract;
    this.withdrawNonce = withdrawNonce;
    this.symbol = symbol;
    this.withdrawMinimum = withdrawMinimum;
    this.withdrawalSum = withdrawalSum;
    this.maximumWithdrawalPerEpoch = maximumWithdrawalPerEpoch;
    this.oracleNonce = oracleNonce;
    this.currentEpoch = currentEpoch;
    this.epochs = epochs;
    this.oracleTimestamp = oracleTimestamp;
  }

  static ByocOutgoingContractState migrateState(StateAccessor oldState) {
    BlockchainAddress largeOracleContract =
        oldState.get("largeOracleContract").blockchainAddressValue();
    EthereumAddress byocContract = EthereumAddress.fromStateAccessor(oldState.get("byocContract"));
    String symbol = oldState.get("symbol").stringValue();
    long withdrawNonce = oldState.get("withdrawNonce").longValue();
    Unsigned256 withdrawMinimum = oldState.get("withdrawMinimum").cast(Unsigned256.class);
    Unsigned256 withdrawalSum = oldState.get("withdrawalSum").cast(Unsigned256.class);
    Unsigned256 maximumWithdrawalPerEpoch =
        oldState.get("maximumWithdrawalPerEpoch").cast(Unsigned256.class);
    long oracleNonce = oldState.get("oracleNonce").longValue();
    long currentEpoch = oldState.get("currentEpoch").longValue();
    AvlTree<Long, Epoch> epochs =
        migrateAvlTree(oldState.get("epochs"), StateAccessor::longValue, Epoch::fromStateAccessor);
    long oracleTimestamp = oldState.get("oracleTimestamp").longValue();

    return new ByocOutgoingContractState(
        largeOracleContract,
        byocContract,
        symbol,
        withdrawNonce,
        withdrawMinimum,
        withdrawalSum,
        maximumWithdrawalPerEpoch,
        oracleNonce,
        currentEpoch,
        epochs,
        oracleTimestamp);
  }

  static <@ImmutableTypeParameter K extends Comparable<K>, @ImmutableTypeParameter V>
      AvlTree<K, V> migrateAvlTree(
          StateAccessor accessor,
          Function<StateAccessor, K> getKey,
          Function<StateAccessor, V> getValue) {
    AvlTree<K, V> result = AvlTree.create();
    for (StateAccessorAvlLeafNode entry : accessor.getTreeLeaves()) {
      K key = getKey.apply(entry.getKey());
      V value = getValue.apply(entry.getValue());
      result = result.set(key, value);
    }

    return result;
  }

  /**
   * Get the address of the large oracle contract.
   *
   * @return address of the large oracle contract
   */
  public BlockchainAddress getLargeOracleContract() {
    return largeOracleContract;
  }

  /**
   * Get the BYOC twin coin symbol.
   *
   * @return the symbol of the BYOC twin
   */
  public String getSymbol() {
    return symbol;
  }

  /**
   * Get the ethereum address of the BYOC contract.
   *
   * @return ethereum address of the BYOC contract
   */
  public EthereumAddress getByocContract() {
    return byocContract;
  }

  /**
   * Get the identities of the currently active small oracles.
   *
   * @return a list of {@link BlockchainAddress} of the current small oracles
   */
  public List<BlockchainAddress> getOracleNodes() {
    return getOracleNodes(currentEpoch);
  }

  List<BlockchainAddress> getOracleNodes(long oracleNonce) {
    return epochs.getValue(oracleNonce).getOracles();
  }

  /**
   * Get the oracle members.
   *
   * @return a list of {@link OracleMember} for the currently active small oracle
   */
  public List<OracleMember> getOracleMembers() {
    return epochs.getValue(currentEpoch).getOracleMembers();
  }

  /**
   * Get the current withdraw nonce.
   *
   * @return the withdraw nonce
   */
  public long getWithdrawNonce() {
    return withdrawNonce;
  }

  /**
   * Get all withdrawals for the current oracle.
   *
   * @return a tree of the withdrawal.
   */
  public AvlTree<Long, Withdrawal> getWithdrawals() {
    return epochs.getValue(oracleNonce).getWithdrawals();
  }

  /**
   * Get the minimum amount that can be withdrawn.
   *
   * @return withdraw minimum
   */
  public Unsigned256 getWithdrawMinimum() {
    return withdrawMinimum;
  }

  /**
   * Get all disputes.
   *
   * @param oracleNonce nonce of the oracle that is disputed
   * @param withdrawNonce nonce of the withdrawal that is disputed
   * @return map from nonce to dispute
   */
  public Dispute getDispute(long oracleNonce, long withdrawNonce) {
    return epochs.getValue(oracleNonce).getDisputes().getValue(withdrawNonce);
  }

  /**
   * Get the sum of withdrawals for the current epoch.
   *
   * @return withdrawal sum
   */
  public Unsigned256 getWithdrawalSum() {
    return withdrawalSum;
  }

  /**
   * Get the maximum amount of BYOC twins allowed to be withdrawn per epoch.
   *
   * @return the total withdrawal amount
   */
  public Unsigned256 getMaximumWithdrawalPerEpoch() {
    return maximumWithdrawalPerEpoch;
  }

  /**
   * Get nonce of the current oracle.
   *
   * @return nonce of the current oracle
   */
  public long getOracleNonce() {
    return oracleNonce;
  }

  /**
   * Get the past epochs of nonces and oracles responsible for those withdrawals.
   *
   * @return a list of epochs
   */
  public AvlTree<Long, Epoch> getEpochs() {
    return epochs;
  }

  /**
   * Create a new pending withdrawal.
   *
   * @param requestingTransaction hash of the originating transaction requesting the withdrawal
   * @param receiver the ethereum address which the coins should be sent to
   * @param amount the amount of coins to send before tax
   * @return the new state containing the new withdrawal
   */
  public ByocOutgoingContractState createPendingWithdrawal(
      Hash requestingTransaction, EthereumAddress receiver, Unsigned256 amount) {
    Withdrawal withdrawal = new Withdrawal(receiver, amount, 3, requestingTransaction);
    return new ByocOutgoingContractState(
        largeOracleContract,
        byocContract,
        symbol,
        withdrawNonce + 1,
        withdrawMinimum,
        withdrawalSum.add(amount),
        maximumWithdrawalPerEpoch,
        oracleNonce,
        currentEpoch,
        epochs.set(
            currentEpoch,
            epochs.getValue(currentEpoch).addPendingWithdrawal(withdrawNonce, withdrawal)),
        oracleTimestamp);
  }

  /**
   * Get a pending withdrawal from the state, matching the nonce, receiver and amount.
   *
   * @param oracleNonce oracle responsible for withdrawal
   * @param nonce the nonce used to find the withdrawal
   * @return the found withdrawal
   */
  public Withdrawal getPendingWithdrawal(long oracleNonce, long nonce) {
    Withdrawal withdrawal = epochs.getValue(oracleNonce).getWithdrawals().getValue(nonce);
    return withdrawal;
  }

  /**
   * Set a new active dispute.
   *
   * @param withdrawId the disputed withdrawal
   * @param dispute the new active dispute
   * @return a state with an active dispute
   */
  public ByocOutgoingContractState createDispute(WithdrawId withdrawId, Dispute dispute) {
    return new ByocOutgoingContractState(
        largeOracleContract,
        byocContract,
        symbol,
        withdrawNonce,
        withdrawMinimum,
        withdrawalSum,
        maximumWithdrawalPerEpoch,
        oracleNonce,
        currentEpoch,
        epochs.set(
            withdrawId.oracleNonce(),
            epochs
                .getValue(withdrawId.oracleNonce())
                .createDispute(withdrawId.withdrawalNonce(), dispute)),
        oracleTimestamp);
  }

  /**
   * Add counter-claim to current dispute being voted on.
   *
   * @param disputeId the id of the disputed transaction
   * @param transaction transaction on external chain
   * @return updated state with new counter-claim
   */
  public ByocOutgoingContractState addCounterClaim(
      WithdrawId disputeId, Dispute.Transaction transaction) {
    return new ByocOutgoingContractState(
        largeOracleContract,
        byocContract,
        symbol,
        withdrawNonce,
        withdrawMinimum,
        withdrawalSum,
        maximumWithdrawalPerEpoch,
        oracleNonce,
        currentEpoch,
        epochs.set(
            disputeId.oracleNonce(),
            epochs
                .getValue(disputeId.oracleNonce())
                .addCounterClaim(disputeId.withdrawalNonce(), transaction)),
        oracleTimestamp);
  }

  /**
   * Archive an active dispute.
   *
   * @param disputeId id of the dispute to archive
   * @param votingResult the claim which has been voted as the truth
   * @return a new state where the active dispute has been archived
   */
  public ByocOutgoingContractState archiveActiveDispute(WithdrawId disputeId, int votingResult) {
    return new ByocOutgoingContractState(
        largeOracleContract,
        byocContract,
        symbol,
        withdrawNonce,
        withdrawMinimum,
        withdrawalSum,
        maximumWithdrawalPerEpoch,
        oracleNonce,
        currentEpoch,
        epochs.set(
            disputeId.oracleNonce(),
            epochs
                .getValue(disputeId.oracleNonce())
                .archiveDispute(disputeId.withdrawalNonce(), votingResult)),
        oracleTimestamp);
  }

  /**
   * Drop the active dispute.
   *
   * @param oracleNonce nonce of the oracle that is disputed
   * @param withdrawal nonce of the withdrawal that is disputed
   * @return a new state with the active dispute dropped
   */
  public ByocOutgoingContractState dropDispute(long oracleNonce, long withdrawal) {
    return new ByocOutgoingContractState(
        largeOracleContract,
        byocContract,
        symbol,
        withdrawNonce,
        withdrawMinimum,
        withdrawalSum,
        maximumWithdrawalPerEpoch,
        oracleNonce,
        currentEpoch,
        epochs.set(oracleNonce, epochs.getValue(oracleNonce).dropDispute(withdrawal)),
        oracleTimestamp);
  }

  /**
   * End the current epoch. Disable oracles, until it has been updated by the large oracle.
   *
   * @return a state with a new epoch and no current oracle nodes
   */
  public ByocOutgoingContractState endCurrentEpoch() {
    long nextEpoch = currentEpoch + 1;
    return new ByocOutgoingContractState(
        largeOracleContract,
        byocContract,
        symbol,
        0,
        withdrawMinimum,
        Unsigned256.ZERO,
        maximumWithdrawalPerEpoch,
        oracleNonce,
        nextEpoch,
        epochs
            .set(currentEpoch, epochs.getValue(currentEpoch).endEpoch())
            .set(nextEpoch, Epoch.create()),
        oracleTimestamp);
  }

  /**
   * Start a new epoch be updating the oracle nodes.
   *
   * @param newOracles the new oracle nodes
   * @param timestamp timestamp of the new oracle
   * @return the new state with the new oracles
   */
  public ByocOutgoingContractState startNewEpoch(List<OracleMember> newOracles, long timestamp) {
    long enabledOracle = this.oracleNonce + 1;
    return new ByocOutgoingContractState(
        largeOracleContract,
        byocContract,
        symbol,
        withdrawNonce,
        withdrawMinimum,
        withdrawalSum,
        maximumWithdrawalPerEpoch,
        enabledOracle,
        currentEpoch,
        epochs.set(enabledOracle, epochs.getValue(enabledOracle).setOracles(newOracles)),
        timestamp);
  }

  /**
   * Add a new signature to a pending withdrawal.
   *
   * @param from the node sending the signature
   * @param oracleNonce the nonce of the oracle responsible for the withdrawal
   * @param nonce the withdraw nonce
   * @param signature the signature to add
   * @return the updated state
   */
  public ByocOutgoingContractState addSignature(
      BlockchainAddress from, long oracleNonce, long nonce, Signature signature) {
    return new ByocOutgoingContractState(
        largeOracleContract,
        byocContract,
        symbol,
        withdrawNonce,
        withdrawMinimum,
        withdrawalSum,
        maximumWithdrawalPerEpoch,
        oracleNonce,
        currentEpoch,
        epochs.set(oracleNonce, epochs.getValue(oracleNonce).addSignature(from, nonce, signature)),
        oracleTimestamp);
  }

  /**
   * Get the currently active epoch. That is the epoch currently getting assigned to new
   * withdrawals.
   *
   * @return the nonce of the current epoch
   */
  public long getCurrentEpoch() {
    return currentEpoch;
  }

  /**
   * If we are awaiting a new oracle from the large oracle.
   *
   * @return true if we are waiting for new oracles.
   */
  public boolean hasPendingUpdate() {
    return oracleNonce != currentEpoch;
  }

  /**
   * Returns the timestamp of the small oracle.
   *
   * @return timestamp
   */
  public long getOracleTimestamp() {
    return oracleTimestamp;
  }

  Hash messageDigest(long oracleNonce, long nonce, Withdrawal withdrawal) {
    return Hash.create(
        stream -> {
          byocContract.write(stream);
          stream.writeLong(oracleNonce);
          stream.writeLong(nonce);
          withdrawal.getReceiver().write(stream);
          withdrawal.getAmount().write(stream);
        });
  }
}
