package com.partisiablockchain.governance.byocoutgoing;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/** Class representing epochs on the state. */
@Immutable
public final class Epoch implements StateSerializable {

  private final AvlTree<Long, Withdrawal> withdrawals;
  private final AvlTree<Long, Dispute> disputes;
  private final FixedList<OracleMember> oracles;
  private final Hash merkleTree;

  @SuppressWarnings("unused")
  Epoch() {
    withdrawals = null;
    disputes = null;
    oracles = null;
    merkleTree = null;
  }

  private Epoch(
      AvlTree<Long, Withdrawal> withdrawals,
      AvlTree<Long, Dispute> disputes,
      FixedList<OracleMember> oracles,
      Hash merkleTree) {
    this.withdrawals = withdrawals;
    this.disputes = disputes;
    this.oracles = oracles;
    this.merkleTree = merkleTree;
  }

  /**
   * Create a new epoch.
   *
   * @return The created epoch
   */
  public static Epoch create() {
    return new Epoch(AvlTree.create(), AvlTree.create(), null, null);
  }

  static Epoch fromStateAccessor(StateAccessor accessor) {
    AvlTree<Long, Withdrawal> withdrawals =
        ByocOutgoingContractState.migrateAvlTree(
            accessor.get("withdrawals"), StateAccessor::longValue, Withdrawal::fromStateAccessor);
    AvlTree<Long, Dispute> disputes =
        ByocOutgoingContractState.migrateAvlTree(
            accessor.get("disputes"), StateAccessor::longValue, Dispute::fromStateAccessor);
    StateAccessor oraclesAccessor = accessor.get("oracles");
    FixedList<OracleMember> oracles = null;
    if (!oraclesAccessor.isNull()) {
      oracles =
          FixedList.create(
              accessor.get("oracles").getListElements().stream()
                  .map(OracleMember::fromStateAccessor));
    }
    Hash merkleTree = accessor.get("merkleTree").cast(Hash.class);
    return new Epoch(withdrawals, disputes, oracles, merkleTree);
  }

  /**
   * Get the oracles of the epoch.
   *
   * @return a list of oracles
   */
  public List<BlockchainAddress> getOracles() {
    return getOracleMembers().stream().map(OracleMember::getIdentity).collect(Collectors.toList());
  }

  /**
   * Get the merkle tree representing the withdrawals that took place on PBC during this epoch.
   *
   * @return the merkle tree root
   */
  public Hash getMerkleTree() {
    return merkleTree;
  }

  /**
   * Set the members of the oracle responsible for this epoch.
   *
   * @param oracles the members of the oracle
   * @return updated epoch
   */
  public Epoch setOracles(List<OracleMember> oracles) {
    return new Epoch(withdrawals, disputes, FixedList.create(oracles), merkleTree);
  }

  /**
   * Add a signature to a withdrawal.
   *
   * @param from the node sending the signature
   * @param nonce the withdraw nonce
   * @param signature the signature to add
   * @return updated epoch
   */
  public Epoch addSignature(BlockchainAddress from, long nonce, Signature signature) {
    int nodeIndex = getNodeIndex(from);
    Withdrawal withdrawal = withdrawals.getValue(nonce);
    return new Epoch(
        withdrawals.set(nonce, withdrawal.addSignature(nodeIndex, signature)),
        disputes,
        oracles,
        merkleTree);
  }

  int getNodeIndex(BlockchainAddress node) {
    return getOracleMembers().stream()
        .map(OracleMember::getIdentity)
        .collect(Collectors.toList())
        .indexOf(node);
  }

  List<OracleMember> getOracleMembers() {
    if (oracles == null) {
      return List.of();
    } else {
      return List.copyOf(oracles);
    }
  }

  /**
   * Add a new withdrawal to this epoch.
   *
   * @param withdrawalNonce the nonce of the withdrawal to add
   * @param withdrawal the withdrawal
   * @return updated epoch
   */
  public Epoch addPendingWithdrawal(long withdrawalNonce, Withdrawal withdrawal) {
    AvlTree<Long, Withdrawal> withdrawals = this.withdrawals.set(withdrawalNonce, withdrawal);
    return new Epoch(withdrawals, disputes, oracles, merkleTree);
  }

  /**
   * Archive a dispute with a result.
   *
   * @param withdrawal the id of the disputed withdrawal
   * @param votingResult the result of the dispute
   * @return updated epoch
   */
  public Epoch archiveDispute(long withdrawal, int votingResult) {
    return new Epoch(
        withdrawals,
        disputes.set(withdrawal, disputes.getValue(withdrawal).votingResult(votingResult)),
        oracles,
        merkleTree);
  }

  /**
   * Drop an active dispute.
   *
   * @param withdrawal the id of the disputed withdrawal
   * @return updated epoch
   */
  public Epoch dropDispute(long withdrawal) {
    return new Epoch(withdrawals, disputes.remove(withdrawal), oracles, merkleTree);
  }

  /**
   * Create a new dispute.
   *
   * @param withdrawal if of the withdrawal to dispute
   * @param dispute the actual dispute
   * @return updated epoch
   */
  public Epoch createDispute(long withdrawal, Dispute dispute) {
    if (disputes.containsKey(withdrawal)) {
      throw new IllegalStateException("Withdrawal has already been disputed");
    }
    return new Epoch(withdrawals, disputes.set(withdrawal, dispute), oracles, merkleTree);
  }

  /**
   * Add a counter-claim to an existing dispute.
   *
   * @param withdrawal the id of the disputed transaction
   * @param transaction the counter-claim to add
   * @return updated epoch
   */
  public Epoch addCounterClaim(long withdrawal, Dispute.Transaction transaction) {
    return new Epoch(
        withdrawals,
        disputes.set(withdrawal, disputes.getValue(withdrawal).addCounterClaim(transaction)),
        oracles,
        merkleTree);
  }

  /**
   * Get all withdrawals within this epoch.
   *
   * @return tree of withdrawals
   */
  public AvlTree<Long, Withdrawal> getWithdrawals() {
    return withdrawals;
  }

  /**
   * Get all disputes within this epoch.
   *
   * @return tree of disputes
   */
  public AvlTree<Long, Dispute> getDisputes() {
    return disputes;
  }

  /**
   * End this epoch.
   *
   * @return updated epoch
   */
  public Epoch endEpoch() {
    return new Epoch(withdrawals, disputes, oracles, createMerkleTreeOfWithdrawals());
  }

  private Hash createMerkleTreeOfWithdrawals() {
    List<byte[]> elements = new ArrayList<>();
    for (Long nonce : withdrawals.keySet()) {
      Withdrawal withdrawal = withdrawals.getValue(nonce);
      byte[] amount =
          SafeDataOutputStream.serialize(stream -> withdrawal.getAmount().write(stream));
      byte[] serializedWithdrawal =
          SafeDataOutputStream.serialize(
              stream -> {
                stream.writeLong(nonce);
                byte[] receiver = withdrawal.getReceiver().getIdentifier();
                stream.write(receiver);
                stream.write(amount);
              });
      elements.add(serializedWithdrawal);
    }
    return MerkleTree.buildTreeFrom(elements);
  }
}
