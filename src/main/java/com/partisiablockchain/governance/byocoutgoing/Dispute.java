package com.partisiablockchain.governance.byocoutgoing;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A claim to dispute that a certain withdrawal of BYOC twins is fraudulent. Consists of both the
 * claimant, the original claim and counter-claims if such have been presented.
 */
@Immutable
public final class Dispute implements StateSerializable {

  private final BlockchainAddress challenger;
  private final FixedList<Transaction> claims;
  private final Integer votingResult;

  @SuppressWarnings("unused")
  Dispute() {
    this.challenger = null;
    this.claims = null;
    this.votingResult = null;
  }

  private Dispute(
      BlockchainAddress challenger, FixedList<Transaction> claims, Integer votingResult) {
    this.challenger = challenger;
    this.claims = claims;
    this.votingResult = votingResult;
  }

  /**
   * Create a new dispute with an original claim of a fraudulent ethereum transaction.
   *
   * @param challenger the address of the account making the claim
   * @param transaction the disputed external transaction
   * @return a new dispute
   */
  public static Dispute create(BlockchainAddress challenger, Transaction transaction) {
    FixedList<Transaction> claims = FixedList.create(List.of(transaction));
    return new Dispute(challenger, claims, null);
  }

  static Dispute fromStateAccessor(StateAccessor accessor) {
    BlockchainAddress challenger = accessor.get("challenger").blockchainAddressValue();
    List<Transaction> claims =
        accessor.get("claims").getListElements().stream()
            .map(Transaction::fromStateAccessor)
            .toList();
    Integer votingResult =
        accessor.get("votingResult").isNull() ? null : accessor.get("votingResult").intValue();

    return new Dispute(challenger, FixedList.create(claims), votingResult);
  }

  /**
   * Get challenger of dispute.
   *
   * @return address of challenger
   */
  public BlockchainAddress getChallenger() {
    return challenger;
  }

  /**
   * Get all claims.
   *
   * @return list of transactions
   */
  public List<Transaction> getClaims() {
    return new ArrayList<>(claims);
  }

  /**
   * Get voting result.
   *
   * @return voting result
   */
  public Integer getVotingResult() {
    return votingResult;
  }

  /**
   * Create dispute from voting result.
   *
   * @param result to dispute
   * @return created dispute
   */
  public Dispute votingResult(int result) {
    return new Dispute(challenger, claims, result);
  }

  /**
   * Add a new counter-claim to this dispute.
   *
   * @param transaction the countering external transaction
   * @return the new dispute with the counter-claim added
   */
  public Dispute addCounterClaim(Transaction transaction) {
    if (Objects.requireNonNull(claims).contains(transaction)) {
      return this;
    } else {
      return new Dispute(
          challenger, Objects.requireNonNull(claims).addElement(transaction), votingResult);
    }
  }

  /**
   * A class representing a BYOC transaction.
   *
   * @param receiver the receiver of the BYOC transaction.
   * @param amount the amount of the BYOC transaction.
   * @param bitmask bitmask indicating the specific oracle members that has signed the transaction
   */
  @Immutable
  public record Transaction(EthereumAddress receiver, Unsigned256 amount, int bitmask)
      implements StateSerializable, DataStreamSerializable {

    static Transaction fromStateAccessor(StateAccessor accessor) {
      EthereumAddress receiver = EthereumAddress.fromStateAccessor(accessor.get("receiver"));
      Unsigned256 amount = accessor.get("amount").cast(Unsigned256.class);

      int bitmask = accessor.get("bitmask").intValue();
      return new Transaction(receiver, amount, bitmask);
    }

    /**
     * Get receiver of the BYOC transaction.
     *
     * @return receiver of the BYOC transaction
     */
    public byte[] getReceiver() {
      return Objects.requireNonNull(receiver()).identifier().getData();
    }

    @Override
    public void write(SafeDataOutputStream stream) {
      Objects.requireNonNull(receiver()).write(stream);
      amount().write(stream);
      stream.writeInt(bitmask());
    }
  }

  /**
   * Rpc friendly wrapper for transaction.
   *
   * @param receiver ethereum address in bytes
   * @param amount transaction amount
   * @param bitmask transaction bitmask
   */
  public record TransactionRpc(EthereumAddressRpc receiver, Unsigned256 amount, int bitmask) {
    Dispute.Transaction convert() {
      return new Dispute.Transaction(
          new EthereumAddress(new LargeByteArray(receiver().bytes())), amount(), bitmask());
    }
  }
}
