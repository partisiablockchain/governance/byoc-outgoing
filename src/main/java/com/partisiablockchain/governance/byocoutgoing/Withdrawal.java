package com.partisiablockchain.governance.byocoutgoing;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/** An outgoing transfer pending the oracles' signatures. */
@Immutable
public final class Withdrawal implements StateSerializable {

  private final EthereumAddress receiver;
  private final Unsigned256 amount;
  private final FixedList<Signature> signatures;
  private final Hash requestingTransaction;

  @SuppressWarnings("unused")
  Withdrawal() {
    receiver = null;
    amount = null;
    signatures = null;
    requestingTransaction = null;
  }

  /**
   * Create a new pending withdrawal.
   *
   * @param receiver ethereum address that should receive the withdrawal
   * @param amount the amount of coins to withdraw
   * @param numberOfOracles the number of known oracles which signs the withdrawal
   * @param requestingTransaction hash of the originating transaction requesting the withdrawal
   */
  public Withdrawal(
      EthereumAddress receiver,
      Unsigned256 amount,
      int numberOfOracles,
      Hash requestingTransaction) {
    this(
        receiver,
        amount,
        FixedList.create(Collections.nCopies(numberOfOracles, null)),
        requestingTransaction);
  }

  private Withdrawal(
      EthereumAddress receiver,
      Unsigned256 amount,
      FixedList<Signature> signatures,
      Hash requestingTransaction) {
    this.receiver = receiver;
    this.amount = amount;
    this.signatures = signatures;
    this.requestingTransaction = requestingTransaction;
  }

  static Withdrawal fromStateAccessor(StateAccessor accessor) {
    EthereumAddress receiver = EthereumAddress.fromStateAccessor(accessor.get("receiver"));
    Unsigned256 amount = accessor.get("amount").cast(Unsigned256.class);
    List<Signature> signatures =
        accessor.get("signatures").getListElements().stream()
            .map(StateAccessor::signatureValue)
            .toList();
    Hash requestingTransaction = accessor.get("requestingTransaction").hashValue();
    return new Withdrawal(receiver, amount, FixedList.create(signatures), requestingTransaction);
  }

  /**
   * Get the address of the receiving ethereum account.
   *
   * @return the receiver
   */
  public EthereumAddress getReceiver() {
    return receiver;
  }

  /**
   * Get the amount, in wei, to send.
   *
   * @return the amount
   */
  public Unsigned256 getAmount() {
    Unsigned256 tax = getTax();
    return amount.subtract(tax);
  }

  /**
   * Get the amount of tax that should be deducted from this transaction.
   *
   * @return the tax
   */
  public Unsigned256 getTax() {
    Unsigned256 tax =
        amount
            .multiply(Unsigned256.create(ByocOutgoingContract.BYOC_TAX_PERMIL))
            .divide(Unsigned256.create(1000));
    return tax;
  }

  /**
   * Add an oracle's signature to the pending withdrawal.
   *
   * @param oracleIndex index of the oracle providing the signature
   * @param signature the signature to add to the withdrawal
   * @return a new instance of the withdrawal with the signature added
   */
  Withdrawal addSignature(int oracleIndex, Signature signature) {
    return new Withdrawal(
        receiver, amount, signatures.setElement(oracleIndex, signature), requestingTransaction);
  }

  /**
   * Get the signatures of the withdrawal transaction.
   *
   * @return a list of signatures
   */
  public List<Signature> getSignatures() {
    return new ArrayList<>(signatures);
  }

  /**
   * Get the signers of the withdrawal transaction.
   *
   * @param oracleNodes the oracle members at the time of signing
   * @return addresses of the oracle members that has signed the withdrawal
   */
  public FixedList<BlockchainAddress> getSigners(List<BlockchainAddress> oracleNodes) {
    FixedList<BlockchainAddress> signers = FixedList.create();
    for (int i = 0; i < signatures.size(); i++) {
      if (signatures.get(i) != null) {
        signers = signers.addElement(oracleNodes.get(i));
      }
    }
    return signers;
  }
}
