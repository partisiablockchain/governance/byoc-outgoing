package com.partisiablockchain.governance.byocoutgoing;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.reflect.RpcType;
import com.partisiablockchain.serialization.LargeByteArray;

/**
 * Rpc friendly wrapper type for ethereum addresses.
 *
 * @param bytes the address in bytes
 */
@SuppressWarnings("ArrayRecordComponent")
public record EthereumAddressRpc(@RpcType(size = 20) byte[] bytes) {

  EthereumAddress convert() {
    return new EthereumAddress(new LargeByteArray(bytes));
  }
}
